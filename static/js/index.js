document.onkeydown = function(e) {
  e = e || window.event;
  switch (e.which || e.keyCode) {
    case 13:
      Array.prototype.slice
        .call(document.getElementsByClassName("enter"))
        .forEach(e => e.click());
  }
};

function PageReload(data) {
  window.location.reload();
}

function PageReloadOnSuccess(data) {
  if (data.success == true) {
    window.location = window.location.href;
  } else {
    alert(data.msg);
  }
}

function doPost(
  url,
  data,
  callbackSucces = console.log,
  callbackError = alert
) {
  $.ajax({
    url: url,
    type: "POST",
    data: {
      csrfmiddlewaretoken: document.getElementsByName("csrfmiddlewaretoken")[0]
        .value,
      ...data
    },
    dataType: "json",
    success: callbackSucces,
    error: callbackError
  });
}

function submitConfig(form) {
  let data = new FormData(form);
  data = {
    automation: Boolean(data.get("id_automation")),
    cleanup: Boolean(data.get("id_cleanup"))
  };
  doPost(`${window.location.origin}/config/`, data, PageReload);
}

function login() {
  let data = { username: $("#username").val(), password: $("#password").val() };
  let onSucces = data => {
    if (data["loggedin"]) {
      window.location.reload();
    } else {
      $("#wrongCred").show();
    }
  };

  doPost(`${window.location.origin}/login/`, data, onSucces, () => {
    $("#wrongCred").show();
  });
}

function viewLogs(id, command) {
  doPost(`${window.location.origin}/logs/`, { id: id }, data => {
    if (data.logs != null) {
      $("#logContent").html(
        data.logs.replace(/[\n]/g, "</br>").replace(/[\t]/g, "&nbsp;&nbsp;")
      );
      $("#logTitle").html(command.substring(1, 200));
      $("#logModal").modal("show");
    }
  });
}

function getIlluminaSSHTML(ss) {
  let file = ss
    .replace(/[\n]/g, "</br>")
    .replace(/[\t]/g, "&nbsp;&nbsp;")
    .split("[Data]");
  let rows = file[1].split("</br>").filter(x => x != "");
  return `${file[0]}[Data]</br>
        <table class="table table-bordered table-sm">            
          <tr>
          ${rows[0]
            .split(",")
            .map(x => "<th>" + x + "</th>")
            .join("")}
          </tr>
          ${rows
            .slice(1)
            .map(
              e =>
                "<tr>" +
                e
                  .split(",")
                  .map(x => "<td>" + x + "</td>")
                  .join("") +
                "</tr>"
            )
            .join("")}          
        </table>`;
}

function getPacBioSSHTML(ss) {
  let file = ss.replace(/[\n]/g, "</br>").replace(/[\t]/g, "&nbsp;&nbsp;");
  let rows = file.split("</br>").filter(x => x != "");
  return `
      <table class="table table-bordered table-sm">            
        <tr>
        ${rows[0]
          .split(",")
          .map(x => "<th>" + x + "</th>")
          .join("")}
        </tr>
        ${rows
          .slice(1)
          .map(
            e =>
              "<tr>" +
              e
                .split(",")
                .map(x => "<td>" + x + "</td>")
                .join("") +
              "</tr>"
          )
          .join("")}          
      </table>`;
  return "";
}

function viewSS(fcid, generate = false, dataFolder = null) {
  doPost(
    `${window.location.origin}/samplesheet/`,
    { id: fcid, generate: generate },
    data => {
      if (data.ss != null) {
        let htmlstring =
          data.type === "PacBio"
            ? getPacBioSSHTML(data.ss)
            : getIlluminaSSHTML(data.ss);

        $("#samplesheet").html(htmlstring);
        $("#fcid").html(`Samplesheet for ${fcid}`);
        $("#samplesheetModal").modal("show");
        let e = document.getElementById("saveSS");
        e.style.display =
          generate == true && dataFolder != null ? "block" : "none";
      }
    }
  );
}

function checkAditionalSS(fcid) {
  doPost(`${window.location.origin}/check-additional/`, { id: fcid }, data => {
    if (data.success == true) {
      window.location = window.location.href;
    } else {
      let excep = data.exceptions
        .map(e => {
          return `<div>
              <h6><b>File:</b> ${e[0]}</h6>
              <b>Exceptions Occured:</b>
              </br>
              <span style="color:red">${e[1]}</span>
              </hr>
          </div>`;
        })
        .join("");
      $("#samplesheet").html(excep);
      $("#fcid").html(`Checking Addtional Samplesheets for ${fcid}`);
      $("#samplesheetModal").modal("show");
      document.getElementById("saveSS").style.display = "none";
    }
  });
}

function mergeDirClick(e) {
  doPost(
    `${window.location.origin}/mergedir/`,
    { id: e.id },
    PageReloadOnSuccess
  );
}

function saveSS() {
  let path = window.location.pathname.split("/");
  let fcid = path[path.length - 1];
  doPost(`${window.location.origin}/samplesheet/`, { fcid: fcid });
}

function logout() {
  doPost(`${window.location.origin}/logout/`, null, PageReload);
}

function revokeRecord(id) {
  let confirmed = confirm("Are you sure?");
  if (confirmed) {
    doPost(`${window.location.origin}/revoke/`, { id: id }, PageReload);
  }
}
var toggledXlPattern = false;

function searchQ(searchText) {
  let urlParams = new URLSearchParams(window.location.search);
  var newQuery = "?search=" + searchText;
  urlParams.forEach((value, key) => {
    if (key !== "search") {
      newQuery += `&${key}=${value}`;
    }
  });
  window.location =
    window.location.origin + window.location.pathname + newQuery;
}

function toggleXlPattern() {
  /*
  TODO: Check if this works
  */
  toggledXlPattern = !toggledXlPattern;

  let btn = document.getElementById("XlPattern");
  let lst = Array.prototype.slice.call(
    document.getElementById("xlList").children
  );
  let olst = Array.prototype.slice.call(
    document.getElementById("oList").children
  );
  let patternArray = Array.prototype.slice
    .call(document.getElementById("pList").children)
    .map(x => x.innerText);
  patternArray = patternArray.concat(
    patternArray.map(x => {
      let splits = x.split("_").filter(i => !isNaN(i));
      if (splits.length > 0) {
        return splits[0];
      } else {
        return x;
      }
    })
  );
  if (toggledXlPattern) {
    btn.classList.remove("btn-outline-primary");
    btn.classList.add("btn-primary");

    lst
      .filter(x => patternArray.filter(i => x.id.match(i)).length == 0)
      .forEach(e => {
        e.style.display = "none";
      });

    olst
      .filter(
        x => patternArray.filter(i => x.id.split("$")[0].match(i)).length == 0
      )
      .forEach(e => {
        e.style.display = "none";
      });
  } else {
    btn.classList.remove("btn-primary");
    btn.classList.add("btn-outline-primary");
    lst
      .filter(x => patternArray.filter(i => x.id.match(i)).length == 0)
      .forEach(e => {
        e.style.display = "block";
      });

    olst
      .filter(
        x => patternArray.filter(i => x.id.split("$")[0].match(i)).length == 0
      )
      .forEach(e => {
        e.style.display = "block";
      });
  }
}

function deleteLink(pk) {
  doPost(`${window.location.origin}/delete-link/`, { pk: pk }, PageReload);
}

async function createTickets(fcid) {
  let btn = document.getElementById("ticketButton");
  btn.disabled = true;
  btn.innerHTML = '<i class="far fa-compass fa-spin"></i>';
  doPost(
    `${window.location.origin}/jira/${fcid}`,
    { action: "create" },
    PageReload,
    error => {
      let btn = document.getElementById("ticketButton");
      btn.disabled = false;
      btn.classList.remove("btn-primary");
      btn.classList.add("btn-error");
      btn.innerHTML = "Retry Create Tickets";
    }
  );
}

function retryRecord(pk) {
  doPost(`${window.location.origin}/retry/`, { pk: pk }, PageReload);
}
