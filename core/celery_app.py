from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from celery.task.control import revoke

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings')
from django.conf import settings

app = Celery("core")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()

I = app.control.inspect(settings.WORKERS)

def active():
    if I.active() and I.active().get(settings.WORKERS[0]):
        return I.active().get(settings.WORKERS[0]) 
    return []

def scheduled(): 
    if I.scheduled() and I.scheduled().get(settings.WORKERS[0]):
        return [
            x.get("request") 
            for x in I.scheduled().get(settings.WORKERS[0])
            if x and x.get("request")
        ]
    return []

def get_scheduled_tasks(attempt=0):        
    try:
        return scheduled() + [
            task for task in active()
            if task.get("name") != 'tasks.execute_inner' 
        ]
    except ConnectionResetError:
        if attempt < 10:
            sleep(1)
            return get_scheduled_tasks(attempt+1)
     

def revoke_all(kill=False):  
    for p in scheduled():
        print(f"Revoking {p.get('id')}")
        revoke(p.get('id'), kill) 









