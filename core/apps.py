from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'core'
    verbose_name = 'core'

    def ready(self):
        from core.utils import systems_check
        systems_check()
