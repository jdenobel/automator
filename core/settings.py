from sentry_sdk.integrations.django import DjangoIntegration
import sentry_sdk
import os
import sys
import socket
import docker
import requests

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Add pipelinelib to path -> allows import
sys.path.insert(0, os.path.join(BASE_DIR, "./pipelinelib/"))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
try:
    SECRET_KEY = os.environ['SECRET_KEY']
except KeyError:
    # mannually set enviroment
    with open(os.path.join(BASE_DIR, "scripts/.env_vars"), 'r') as f:
        for line in filter(None, f.readlines()):
            os.environ.update(
                (line.split("export")[-1].strip().split("=", 1),)
            )
        SECRET_KEY = os.environ['SECRET_KEY']

HOSTNAME = socket.gethostname()

LOCAL = HOSTNAME == "rekenmachine"

DEBUG = True
PROD = os.path.realpath(BASE_DIR) == '/mnt/nfs/automator'

ALLOWED_HOSTS = [
    "localhost",
    "10.150.0.103",
    "control01",
    "automator.baseclear.com"
]


CUSTOM_APPS = [
    'core',
    'automator',
]

THIRD_PARTY_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_celery_results',
    'crispy_forms',
    'solo',
    'mozilla_django_oidc'

]

CRISPY_TEMPLATE_PACK = 'bootstrap4'

INSTALLED_APPS = THIRD_PARTY_APPS + CUSTOM_APPS

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'core.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'automator/templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                "core.context_preprocessors.add_meta",
            ],
        },
    },
]

WSGI_APPLICATION = 'core.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases
SQL01_USER = os.environ['SQL01_USER']
SQL01_PASSWORD = os.environ['SQL01_PASSWORD']

SQL01_BASE_SETTING = {
    'ENGINE': 'sql_server.pyodbc',
    'HOST': '10.150.0.15',
    'USER': SQL01_USER,
    'PASSWORD': SQL01_PASSWORD,
    'PORT': '1433',
    'MARS_Connection': False,
    'OPTIONS': {
        'driver': 'ODBC Driver {} for SQL Server'.format(
            '17' if LOCAL else '13'
        ),
    }
}

SQLITE_BASE_SETTING = {
    'ENGINE': 'django.db.backends.sqlite3'
}

ORDER_DATABASE = 'OrderPortal-Test' if not PROD else 'OrderPortal-Prod'
DATABASE = 'Automator-Test' if not PROD else 'Automator'
EASYTRACK_DATABASE = 'EasyTrack-Test' if not PROD else 'EasyTrack'

DATABASES = {
    'default': dict(SQL01_BASE_SETTING, NAME=DATABASE),
    'sql01_master': dict(SQL01_BASE_SETTING, NAME='master'),
    'EasyTrack': dict(SQL01_BASE_SETTING, NAME=EASYTRACK_DATABASE),
    'OrderPortal': dict(SQL01_BASE_SETTING, NAME=ORDER_DATABASE),
    'Celery': dict(
        SQLITE_BASE_SETTING, NAME=os.path.join(
            BASE_DIR, 'scheduler/manager/results.db')
    ),
}


AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGE_CODE = 'en-us'

TIME_ZONE = "Europe/Paris"

USE_I18N = True

USE_L10N = True

USE_TZ = True


BASE_ROOT = ('' if not LOCAL else '/run/user/1000/gvfs/sftp:host=10.150.0.103')

DATA_ROOT = BASE_ROOT + '/mnt/nfs/rawdata/NGS-projects'
PIPELINE_ROOT = BASE_ROOT + '/mnt/nfs/pipelines'
LOGS_ROOT = BASE_ROOT + '/var/log/celery/logs'
ILLUMINA_DATA_ROOT = BASE_ROOT + "/mnt/nfs/rawdata/IlluminaData"
PACBIO_ROOT = BASE_ROOT + '/mnt/nfs/rawdata/PacBioSequel'
NOVASEQ_DATA_ROOT = BASE_ROOT + '/mnt/nfs/rawdata/Novaseq'
ONT_DATA_ROOT = BASE_ROOT + '/mnt/nfs/rawdata/MinION'


STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static'),
]


I_DRIVE = ("/run/user/10006/gvfs/smb-share:server=10.150.0.13,share=algemeen$"
           if not LOCAL else
           "/run/user/1000/gvfs/smb-share:server=10.150.0.13,share=algemeen$") + \
    "/02_Operations/01_Production/02_Analyse/"


MOUNTED_I = os.path.isdir(I_DRIVE)

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'baseclear-nl.mail.protection.outlook.com'
EMAIL_PORT = 25
EMAIL_USE_TLS = True
SERVER_EMAIL = 'BaseClear SMILE <smile@baseclear.nl>'
ADMIN_MAILS = ["jacob.denobel@baseclear.nl"]
WATCHER_MAILS = [
    "jacob.denobel@baseclear.nl",
    "iris.kolder@baseclear.nl",
    "hilde.stawski@baseclear.nl",
    "marco.antonleon@baseclear.nl",
    "marta.terhaar@baseclear.nl",
    "mirna.baak@baseclear.nl"
]

LINUX_USERNAME_MAPPER = {
    "manton": "marco.antonleon@baseclear.nl",
    "jdenobel": "jacob.denobel@baseclear.nl",
    "hstawski": "hilde.stawski@baseclear.nl",
    "mterhaar": "marta.terhaar@baseclear.nl",
    "mbessem": "mark.bessem@baseclear.nl",
    "mbaak": "mirna.baak@baseclear.nl",
    "hilde.stawski": "hilde.stawski@baseclear.nl",
    "mark.bessem": "mark.bessem.baseclear.nl",
    "mirna.baak":  "mirna.baak@baseclear.nl",
    "marco.antonleon":  "marco.antonleon@baseclear.nl",
    "marta.terhaar": "marta.terhaar@baseclear.nl",
}

if not LOCAL:
    #### Docker Conf ####
    DOCKER_CLIENT = docker.from_env()
    NODES = {
        x.attrs.get("ID"): x.attrs.get("Description").get("Hostname")
        for x in DOCKER_CLIENT.nodes.list()
    }
    TOTAL_CPUS = 24
    TOTAL_FAST_CPUS = 48
    FAST_NODES = ("node08.compute.local", "node07.compute.local",)
    VERBOSE = True
    #### Celery Conf ####
    WORKERS = ('celery@control01.compute.local',)
    CELERY_RESULT_BACKEND = 'django-db'
    CELERY_BROKER_URL = f"amqp://guest:guest@node01:7000//"


# keep these secret
OIDC_RP_CLIENT_ID = os.environ['OIDC_RP_CLIENT_ID']
OIDC_RP_CLIENT_SECRET = os.environ['OIDC_RP_CLIENT_SECRET']


OIDC_HOST = "https://baseplaza.baseclear.com/" if PROD else "https://baseplaza-test.baseclear.com/"
OIDC_BASE_ENDPOINT = OIDC_HOST + "oidc/"
OIDC_OP_JWKS_ENDPOINT = OIDC_BASE_ENDPOINT + "jwks"
OIDC_OP_AUTHORIZATION_ENDPOINT = OIDC_BASE_ENDPOINT + "authorize"
OIDC_OP_TOKEN_ENDPOINT = OIDC_BASE_ENDPOINT + "token"
OIDC_OP_USER_ENDPOINT = OIDC_BASE_ENDPOINT + "userinfo"
OIDC_RP_SIGN_ALGO = 'RS256'
OIDC_RP_SCOPES = 'openid profile email baseclear'
OIDC_OP_LOGOUT_URL = OIDC_HOST + 'user/logout/'
OIDC_OP_LOGOUT_URL_METHOD = 'core.oidc.provider_logout'

LOGIN_URL = '/oidc/authenticate/'
LOGIN_REDIRECT_URL = '/'


AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'core.oidc.OIDCAuthentication',
)

# If this raises an error, the sentry docker is down
# GoTo: /home/prod/onpremise
# Then: docker-compose up -d
sentry_project = os.environ['SENTRY_ID']
sentry_key = os.environ['SENTRY_KEY']
sentry_sdk.init(
    dsn=f"http://{sentry_key}@control01:9000/{sentry_project}",
    integrations=[DjangoIntegration()]
)

def et_supports_pools():
    from django.db import connections

    required_tables = {
        'tblNextGenPools',
        'vwNextGenSampleRelationView',
        'tblNextGenPoolSample',
        'vwNextGenFlowSampleRelationView'
    }
    present_tables = set(connections['EasyTrack'].introspection.table_names(include_views=True))

    return required_tables <= present_tables

ET_POOLS_SUPPORTED = et_supports_pools()
