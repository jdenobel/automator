import time
from functools import wraps

from django.contrib import admin
from django.contrib.auth.models import User, Group
from django.conf import settings
from django.db import models
from django.apps import apps
from solo.admin import SingletonModelAdmin

from automator.models.Config import Config


def register_all_models():
    exclusions = [
        "Group", "LogEntry", "ContentType", "Session", "Config"
    ]
    for mod in apps.get_models(include_auto_created=False):
        if mod.__name__ in exclusions:
            continue
        try:
            @admin.register(mod)
            class AdminModel(admin.ModelAdmin):
                list_display_links = list_display = [
                    field.name
                    for field in mod._meta.fields
                ]
        except Exception as error:
            if not str(error).endswith('already registered'):
                print(error)

    admin.site.register(Config, SingletonModelAdmin)


def timeit(method):
    @wraps(method)
    def wrapper(*args, **kwargs):
        start = time.clock()
        result = method(*args, **kwargs)
        end = time.clock()
        print('Execution duration %r --> %.6f s' %
              (method.__name__, (end - start)))
        return result
    return wrapper


def str_to_type(s):
    try:
        return int(s)
    except ValueError:
        try:
            return float(s)
        except ValueError:
            return s


def check_watchers():
    try:
        for email in settings.WATCHER_MAILS:
            user = User.objects.filter(email=email, groups=None).first()
            if not user:
                continue
            user.groups.add(Group.objects.get(name="watchers"))
    except Exception as error:
        print(f"ERROR: {str(error)}")


def systems_check():
    register_all_models()
    check_watchers()
