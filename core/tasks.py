import subprocess
import os
from random import randint
from json import loads
import datetime
from celery.contrib import rdb
from celery.exceptions import Retry
from django.conf import settings
from django.utils import timezone
from .celery_app import app, get_scheduled_tasks
from automator.models import QueueRecord


def cpus(ncpus):
    return int(ncpus * (10**-9))


class BaseTask(app.Task):
    abstract = True
    time = 15

    @property
    def swarm_status(self):
        nodes = {
            node: settings.TOTAL_FAST_CPUS
            if hostname in settings.FAST_NODES else settings.TOTAL_CPUS
            for node, hostname in settings.NODES.items()
        }

        for service in settings.DOCKER_CLIENT.services.list():
            for i in service.tasks({"desired-state": "running"}):
                if not i.get('NodeID'):
                    continue
                if not i.get('Spec').get('Resources').get('Reservations').get('NanoCPUs'):
                    continue
                try:
                    nodes[i['NodeID']] -= cpus(i['Spec']
                                               ['Resources']['Reservations']['NanoCPUs'])
                except KeyError:
                    if self.recursion_lock:
                        raise
                    self.reindex_nodes()
                    return self.swarm_status

        self.recursion_lock = False
        return dict(nodes)

    def reindex_nodes(self):
        settings.NODES = {
            x.attrs.get("ID"): x.attrs.get("Description").get("Hostname")
            for x in settings.DOCKER_CLIENT.nodes.list()
        }
        self.recursion_lock = True

    def get_random_countdown(self, n_retries):
        return randint(1, self.time) + (2.0 ** n_retries)

    def get_priority(self, task):
        if type(task.get("kwargs")) == str:
            return loads(task.get("kwargs").replace("'", '"')).get("priority")
        elif type(task.get("kwargs")) == dict:
            return task.get("kwargs").get("priority")

    def can_run(self, priority, required_cpus):
        self.recursion_lock = False
        if not any(
                node >= required_cpus
                for node in self.swarm_status.values()):
            return False

        if not any(
                self.get_priority(task) < priority
                for task in get_scheduled_tasks()):
            return True

    def on_retry(self, exc, task_id, args, kwargs, einfo):
        print(f"Retrying {task_id} {kwargs}")
        super(BaseTask, self).on_retry(exc, task_id, args, kwargs, einfo)

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        print(f"Failed {task_id} {exc}")
        super(BaseTask, self).on_failure(exc, task_id, args, kwargs, einfo)


@app.task(bind=True)
def execute_inner(self, command, record_id):
    record = QueueRecord.QueueRecord.objects.get(pk=record_id)
    subclass = getattr(record, 'demultiplexingrecord', None) \
        or getattr(record, 'executionrecord', None)

    record = subclass or record
    record.async_code = self.request.id
    record.pipeline_started_at = timezone.now()
    record.save()
    if not record.exit_code == -1:
        with open(record.log_file, "w+") as f:
            p = subprocess.Popen(command, shell=True, stdout=f, stderr=f, preexec_fn=os.setsid, env=dict(
                os.environ, **{"PYTHONUNBUFFERED": "TRUE"}
            ))
            record.pid = p.pid
            record.status = "RUNNING" if record.exit_code != -1 else record.status
            record.save()
            exit_code = p.wait()
            record.pipeline_completed_at = timezone.now()
            record.exit_code = exit_code
            record.status = "SUCCESS" if exit_code == 0 else record.status
            record.save()
            record.final_check()
    return record.exit_code


@app.task(
    bind=True,
    base=BaseTask)
def excecute_system_commmand(self, record_id, command, priority, required_cpus):
    try:
        if self.can_run(priority, required_cpus):
            return execute_inner.delay(command=command, record_id=record_id)
        else:
            self.retry(countdown=self.get_random_countdown(
                self.request.retries), max_retries=float("inf"))
    except Retry:
        raise

    except Exception:
        self.retry(countdown=self.get_random_countdown(
            self.request.retries), max_retries=3)
