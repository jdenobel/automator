from django.conf import settings
from mozilla_django_oidc.auth import OIDCAuthenticationBackend
from mozilla_django_oidc.views import OIDCLogoutView
from automator.models import UserSettings


class OIDCAuthentication(OIDCAuthenticationBackend):
    def create_user(self, claims):
        '''Create user information'''
        user = super().create_user(claims)
        return self.update_user(user, claims)

    def update_user(self, user,  claims):
        '''Update user information'''
        user.username = claims['username']
        user.first_name = claims.get('given_name', '')
        user.last_name = claims.get('family_name', '')
        if not hasattr(user, 'settings') and claims.get("jira_token"):
            UserSettings.objects.update_or_create(
                user=user,
                defaults=dict(
                    ira_token=claims.get("jira_token"),
                    jira_project=claims.get("jira_project")
                )
            )
        user.save()
        return user


class OIDCAuthenticationLogout(OIDCLogoutView):
    def get(self, request):
        '''Base class doesn't have a get, overwrite'''
        return super().post(request)


def provider_logout(req):
    '''Build logout return url'''
    return (
        f'{settings.OIDC_OP_LOGOUT_URL}'
        f'?next={req.scheme}://{req.get_host()}/'
    )
