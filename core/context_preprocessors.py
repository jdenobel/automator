import os
import random
from django.conf import settings
from automator.forms import ConfigForm
from automator.models import Config

QUOTES = [
    "Oh, I'm sorry, did I break your concentration?",
    "Where is my supa suit? WHERE. IS. MY. SUPA. SUIT?! You tell me where my suit is, woman! It's for the greater good!",
    "Mmm-mmmmm. This IS a tasty burger.",
    "And you will know my name is the Lord when I lay my vengeance upon thee.",
    "Hold on to ya butts.",
    "First, We're Going To Seal Off This Pool—",
    "I Hate This Hacker Crap!",
    "This Party's Over",
    "We're All Gonna Be Like Three Little Fonzies Here.",
    "Zeus! You Got A Problem With That?",
    "I Eat Every Motherf*ckin' Thing.",
    "Given That It's A Stupid-Ass Decision, I've Elected To Ignore It.",
    "You Been Taking Them Dick Pills Again?",
    "AK-47. The Very Best There Is.",
    "Yes, They Deserved To Die And I Hope They Burn In Hell!",
    "My Duty Is To Please That Booty.",
    "Everyone Knows When You Make An Assumption, You Make An Ass Out Of 'U' And 'Umption.'",
    "English, Motherf*cker, Do You Speak It?",
    "I Have Had It With These Motherf*cking Snakes On This Motherf*cking Plane!"
]


def add_meta(request):
    return {
        "has_i": os.path.isdir(settings.I_DRIVE),
        "config_form": ConfigForm(instance=Config.get_solo()),
        "quote": random.choice(QUOTES),
        "sentry_url_suffix": "automator" if settings.PROD else "automator-dev"

    }
