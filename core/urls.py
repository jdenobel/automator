from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import RedirectView
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from core.oidc import OIDCAuthenticationLogout
from automator import views


urlpatterns = [
    path('', RedirectView.as_view(url='queue/')),
    path('processes/', views.execution, name="execution"),
    path('pipelines/', views.pipelines, name="pipelines"),
    path('pipelines/<int:pk>', views.pipelines, name="pipelines"),
    path('orders/', views.orders, name="orders"),
    path('queue/', views.queue, name="queue"),
    path('flowcell/', views.flowcell, name="flowcell"),
    path('flowcell/<int:pk>', views.flowcell_details, name="flowcell_details"),
    path('jira/<int:pk>', views.jira, name="jira"),
    path('queue/<int:pk>', views.queue_details),
    path('file/', views.serve_static, name="file"),
    path('download/<path>/<int:pk>/', views.downloadfile, name="download"),
    path('delete-link/', views.delete_link),
    path('logs/', views.logfile),
    path('config/', views.update_config),
    path('samplesheet/', views.samplesheet),
    path('check-additional/', views.check_additional_samplesheets),
    path('mergedir/', views.mergedir),
    path('admin/', admin.site.urls),
    path('revoke/', views.revoke),
    path('gantt/', views.get_lastest_runs, name='gantt'),
    path('logout/', OIDCAuthenticationLogout.as_view(), name='logout'),
    path('oidc/', include('mozilla_django_oidc.urls')),
    path('retry/', views.retry),
] + staticfiles_urlpatterns()
