#!/usr/bin/env bash
CELERYD_NODES="celery"
CELERY_BIN="/usr/local/bin/celery"
CELERY_APP="core"
CELERYD_PID_FILE="/var/log/celery/%n.pid"
CELERYD_LOG_FILE="/var/log/celery/%n.log"
CELERYD_LOG_LEVEL="INFO"
CELERYD_STATE_DB="/var/log/celery/%n.state"
CELERYD_OPTS="-P eventlet -c 500 -Ofair --statedb=${CELERYD_STATE_DB}"
CELERYD_CHDIR="/mnt/nfs/automator"
export DJANGO_SETTINGS_MODULE='core.settings'

cd ${CELERYD_CHDIR}
case "$1" in
        start)
            ${CELERY_BIN} multi start -A ${CELERY_APP} --pidfile=${CELERYD_PID_FILE} \
              --logfile=${CELERYD_LOG_FILE} --loglevel=${CELERYD_LOG_LEVEL} ${CELERYD_OPTS}
            ;;
        stop)
            ${CELERY_BIN} multi stopwait ${CELERYD_NODES} --pidfile=${CELERYD_PID_FILE}
            ;;

	single)
		${CELERY_BIN} worker -A ${CELERY_APP} --pidfile=${CELERYD_PID_FILE} \
              		--logfile=${CELERYD_LOG_FILE} --loglevel=${CELERYD_LOG_LEVEL} ${CELERYD_OPTS} &     
	;;
        restart)
            ${CELERY_BIN} multi restart ${CELERYD_NODES} -A ${CELERY_APP} --pidfile=${CELERYD_PID_FILE} \
             --logfile=${CELERYD_LOG_FILE} --loglevel=${CELERYD_LOG_LEVEL} ${CELERYD_OPTS}
            ;;         
        *)
            echo $"Usage: $0 {start|stop|restart}"
            exit 1 
esac
