import os
import pickle
import pytz
from time import time
from datetime import timedelta, datetime
from itertools import chain
from django.conf import settings
from django.utils import dateformat
from django.shortcuts import render, get_object_or_404, redirect
from django.http import JsonResponse, HttpResponse
from django.utils import timezone
from django.core.exceptions import PermissionDenied
from django.contrib.auth import authenticate, login, logout
from django.core.paginator import PageNotAnInteger, Paginator
from django.views.static import serve
from django.db.models import Q, F
from django.contrib.auth.decorators import login_required

from automator.models import (
    ExecutionRecord,
    EasyTrackOrder,
    FlowCellSample,
    QueueRecord,
    FlowCell,
    PlateToExcel,
    DemultiplexingRecord,
    MergeDirectory,
    Pipeline,
    ETToken,
    ILLUMINA_TYPES,
    Config
)

from core.tasks import excecute_system_commmand
from .forms import PipelineForm, ConfigForm


def save_form(request, formtype, instance):
    '''Generic function for saving a form
    Only used for saving the ConfigForm and the
    Pipeline form in the current setup

    Parameters
    ----------
    request : WSGIRequest
    formtype : Form class type
    instance : Form class instance

    Returns
    -------
    form
        a form of type formtype propagated with the provided instance
    '''
    if request.method == "POST":
        form = formtype(request.POST, instance=instance)
        if form.is_valid():
            instance = form.save()
            instance.save()
        form = formtype(instance=instance)
    else:
        form = formtype(instance=instance)
    return form


@login_required
def update_config(request):
    '''A view for updating the Global config
    call save form, meant for using with Ajax requests

    Parameters
    ----------
    request : WSGIRequest

    Returns
    -------
    JsonResponse
        If no exception occurs, succes is returned

    '''
    instance = Config.get_solo()
    save_form(request, ConfigForm, instance)
    return JsonResponse({"succes": True})


def pipelines(request, pk=None):
    '''A view for updating a Pipeline instance
    call save form, meant for using with Ajax requests

    Parameters
    ----------
    request : WSGIRequest
    pk : int, optional
        a primary key for an already defined pipeline,
        used in updating an pipeline instance

    Returns
    -------
    JsonResponse
        If no exception occurs, succes is returned
    '''
    instance = None if not pk else get_object_or_404(Pipeline, pk=pk)
    form = save_form(request, PipelineForm, instance)
    return render(request, "pipelines.html",
                  {
                      "form": form,
                      'records': get_paginator(request, Pipeline.objects.order_by("pk"))
                  }
                  )


def get_paginator(request, records):
    '''Helper function for wrapping a Paginator around a Queryset

    Parameters
    ----------
    request : WSGIRequest
    records : a Django Queryset

    Returns
    -------
    Paginator
        The Queryset chunked in pages of size 10
    '''
    return Paginator(records, 10)\
        .get_page(request.GET.get('page'))


@login_required
def jira(request, pk):
    '''A view for fetching or creating Jira tickets
    Meant for using with Ajax requests. If the request
    method is POST tickets will be created for a flowcell.
    Otherwise, they are fetched.

    Parameters
    ----------
    request : WSGIRequest
    pk : int, optional
        a primary key for a flowcell

    Returns
    -------
    JsonResponse
        If no exception occurs, a Json object with the Jira tickets
        returned
    '''
    record = get_object_or_404(FlowCell, volgnummer=pk)
    if request.POST.get("action") == "create":
        tickets = record.generate_tickets(request.user)
    else:
        tickets = record.get_tickets(request.user)
    return JsonResponse({
        "tickets": [x.key for x in tickets]
    })


def queue_details(request, pk):
    '''A view for viewing the queue page with a single record.

    Parameters
    ----------
    request : WSGIRequest
    pk : int, optional
        a primary key for a QeueRecord

    Returns
    -------
    render
        Containing a single record in the list of records.
    '''
    return render(request, "queue.html", {
        'records': QueueRecord.objects.filter(pk=pk)
    })


def execution(request):
    '''A view for viewing the Automated Processes page.
     Fetches all exectution records and demux record,
     sorts them by start date/time.

    Parameters
    ----------
    request : WSGIRequest

    Returns
    -------
    render
        Containing a paginated list of records.
    '''
    ex_records = ExecutionRecord.objects.all().order_by("-pipeline_started_at")
    demux_records = DemultiplexingRecord.objects.filter(
        automatic=True).order_by("-pipeline_started_at")
    qs = sorted(chain(ex_records, demux_records), reverse=True, key=lambda x:
                int((x.pipeline_started_at or timezone.now()).timestamp())
                )
    return render(request, 'execution.html', {
        'records': get_paginator(request, qs)
    })


def queue(request):
    '''A view for viewing the Qeue Page
     Depending on the query parameters provided in the
     GET request, a (sub)set QueueRecords is returned to
     the interface in a paginated manner

    Parameters
    ----------
    request : WSGIRequest

    Returns
    -------
    render
        Containing a paginated list of records. Also includes
        attional meta information in the response context. This
        is used for rendering the html
    '''
    qs = QueueRecord.objects.all().order_by("-id")
    search_query = request.GET.get("search") or ''
    status = request.GET.get("status") or ''
    active = qs.exclude(pipeline_started_at=None)\
        .filter(pipeline_completed_at=None)
    qs = active if status == 'active' else qs
    if search_query != '':
        qs = qs.filter(command__icontains=search_query)
    return render(request, 'queue.html', {
        "search_query": search_query,
        'records': get_paginator(request, qs),
        'currently_running': active.count(),
        'status': status,
    })


def create_plate_to_excel(flowcell, filename, plate_id):
    '''Helper function for creating a PlateToExcel object.
    These objects are used for determining which excel sheets
    are to be used when starting a demux pipeline.

    Parameters
    ----------
    flowcell : FlowCell
        an instance of the FlowCell model
    filename : str
        the name of the excel file
    plate_id : str
        the name of the plate in the samplesheet of the FlowCell
    '''
    PlateToExcel.objects.create(
        path=filename,
        name=os.path.basename(filename),
        plate_id=plate_id,
        et_sample_id=plate_id.split("_")[-1],
        target_path=os.path.join(
            flowcell.data_folder, os.path.basename(filename))
    )


def execute_demultiplexing(fc):
    '''Helper method for excecuting a Demux directly

    Parameters
    ----------
    fc: FlowCell
        an instance of the FlowCell model
    '''
    DemultiplexingRecord.objects\
        .create(flowcell_id=fc.pk, automatic=False)\
        .execute()


@login_required
def flowcell_details(request, pk):
    '''A view for viewing the details page for a specific flowcell.
    Depending on the request method and request parameters
    this view is responsible the actions:
        - viewing details
        - linking excels plates
        - directly excecuting demux
    Parameters
    ----------
    request : WSGIRequest
    pk : int
        a primary key of a FlowCell instance

    Returns
    -------
    render
        Containing meta information, belonging to a
        specific Flowcell

    Raises
    ------
    Http404
        When the pk is not found in the database
    '''
    record = get_object_or_404(FlowCell, volgnummer=pk)
    tickets = [] if not hasattr(request.user, "settings") else \
        request.user.settings.jira_service.get_tickets(record)
    context = {
        'record': record,
        'tickets': tickets
    }
    if request.method == 'POST':
        filename = request.POST.get("file")
        plate_id = request.POST.get("plate")
        flowcell_id = request.POST.get("fcid")
        try:
            if filename and plate_id:
                create_plate_to_excel(record, filename, plate_id)
            elif flowcell_id and int(flowcell_id) == record.pk:
                execute_demultiplexing(record)
            context.update(success=True)
            request.POST = {}
        except Exception as err:
            print(err, type(err))
            context.update(success=False, msg=str(err))

    return render(request, "flowcell_detail.html", context)


def get_lastest_runs(request, window={'days': 3}):
    '''A view for fetching the latest QueueRecords
     For usage with an Ajax request, used to render
     the gantt chard on the Queue page. The window
     determines how old of records are shown.

    Parameters
    ----------
    request : WSGIRequest
    window : dict
        dictionary containing parameters for a timedelta
        instantiation

    Returns
    -------
    JsonResponse
        If no exception occurs, a Json object with the requested
        records is returned
    '''
    window = timezone.now() - timedelta(**window)
    qs = (QueueRecord.objects
          .filter(pipeline_started_at__gt=window)
          .filter(pipeline_completed_at__gte=((F('pipeline_started_at') or timezone.now()) + timedelta(hours=1)))
          .order_by('-pipeline_started_at'))
    return JsonResponse({
        "status": 200,
        "records": [
            {
                "name": f"[{x.assumed_project}]: {x.assumed_name}",
                "start": dateformat.format(x.pipeline_started_at, "U"),
                "end":  dateformat.format(x.pipeline_completed_at or timezone.now(), "U"),
                "status": x.status,
                "exit_code": x.exit_code,
                "id": x.pk,
                "command": x.command_nice,
                "working_dir": x.working_directory,
                "progress": 100,
            } for x in qs
        ]
    })


def serve_static(request, path=None):
    '''A view returning a static file.

    Parameters
    ----------
    request : WSGIRequest
    path : str
        a path of a file

    Returns
    -------
    serve
        django method, helper for serving a static file
    '''
    path = path or request.GET.get("path")
    if not os.path.exists(path):
        raise PermissionDenied('Path does not exists')
    return serve(request,
                 os.path.basename(path),
                 os.path.dirname(path)
                 )


def downloadfile(request, path, pk):
    '''Wrapper function for serve_static, for usage with a FlowCell

    Parameters
    ----------
    request : WSGIRequest
    path : str
        a path of a file
    pk : int
        an primary key of a FlowCell instance

    '''
    record = get_object_or_404(FlowCell, volgnummer=pk)
    path = [x[1] for x in record.xlsx_files if x[0] == path][0]
    return serve_static(request, path)


@login_required
def delete_link(request):
    '''View for deleting a PlateToExcel instance
    Meant for using with Ajax requests.

    Parameters
    ----------
    request : WSGIRequest

    Returns
    -------
    JsonResponse
        If no exception occurs, a Json object is returned 
        with a succes message
    '''
    if request.method == 'POST':
        record = get_object_or_404(
            PlateToExcel, pk=request.POST.get("pk")).delete()
        return JsonResponse({"success": True})


def cached_has_folder(qs):
    '''Function for saving a Queryset of Flowcell objects a pickle
    In order to optimize the page load of the flowcell page,
    this function caches the results of a FlowCell queryset
    every hour in a pickle file. This ensures that the page responds
    quickly.

    Parameters
    ----------
    qs : Queryset

    Returns
    -------
    list
        data requested by the queryset, either cached or freshly
        fetched.
    '''
    cache_file = os.path.join(os.path.dirname(__file__), ".queryset_cache")
    one_hour_ago = timezone.now() - timedelta(hours=1)
    if not os.path.isfile(cache_file) or \
            datetime.fromtimestamp(
                os.path.getmtime(cache_file),
                pytz.timezone(settings.TIME_ZONE)) < one_hour_ago:
        data = [{
            "volgnummer": x.volgnummer,
            "illuminaid_nice": x.illuminaid_nice,
            "status": x.status,
            "typerun": x.typerun,
            "typesequencer": x.typesequencer,
            "instrument_type": x.instrument_type,
            "invoerdatum": x.invoerdatum,
            "rundatum": x.rundatum,
            "is_run_completed": x.is_run_completed,
            "is_demultiplexed": x.is_demultiplexed,
            "opmerkingen": x.opmerkingen,
            "pk": x.pk,
        } for x in qs if x.data_folder]

        with open(cache_file, "wb") as handle:
            pickle.dump(data, handle)
        return data

    with open(cache_file, "rb") as handle:
        return pickle.loads(handle.read())


def flowcell(request):
    '''View function for the flowcell overview page.
    Depending on the parameters provided in the search query
    or the status in the GET parameters. A different
    subset of flowcells is returned.

    Parameters
    ---------
    request : WSGIRequest

    Returns
    -------
    render
        a rendered page request, including some context and
        the paginated list of requested records.
    '''
    status = request.GET.get("status")
    search_query = request.GET.get("search") or ''
    qs = FlowCell.objects.all().order_by("-volgnummer")
    if search_query != '':
        qs = qs.filter(illuminaid__icontains=search_query) |\
            qs.filter(typesequencer__icontains=search_query) |\
            qs.filter(sequencernaam__icontains=search_query) |\
            qs.filter(typerun__icontains=search_query) |\
            qs.filter(volgnummer__icontains=search_query)

    if status == "hasFolder":
        qs = cached_has_folder(qs)
    elif status == 'pacbio':
        qs = qs.filter(sequencernaam='PacBio')
    elif status == 'illumina':
        qs = qs.filter(typesequencer__in=ILLUMINA_TYPES)
    elif status == 'ont':
        qs = qs.filter(sequencernaam="MinIon01")
    elif status == 'other':
        qs = qs.exclude(
            Q(sequencernaam__in=['PacBio', "MinIon01"]) |
            Q(typesequencer__in=ILLUMINA_TYPES)
        )
    elif status:
        qs = qs.filter(status=(None if status == 'Null' else status))

    return render(request, 'flowcell.html', {
        'status': status,
        "total": len(qs),
        "search_query": search_query,
        'records': get_paginator(request, qs)
    })


@login_required
def logfile(request):
    '''View for Fetching the logs of a QueueRecord
    Meant for usage with Ajax, in the frontend this is shown
    in the modal popup.

    Parameters
    ----------
    request : WSGIRequest

    Returns
    -------
    JsonResponse
        If no exception occurs, a Json object is returned 
        with the logs in text format of the requested record.

    Raises
    ------
    Http404
        if the requested record cannot be found
    '''
    return JsonResponse(
        {"logs": get_object_or_404(QueueRecord, pk=request.POST['id']).logs})


@login_required
def mergedir(request):
    '''View creating a MergeDirectory object
    Meant for using with Ajax requests. This is used in a similar
    fashion as the PlateToExcel objects, an is used for constructing
    the demultiplexing command.

    Parameters
    ----------
    request : WSGIRequest

    Returns
    -------
    JsonResponse
        If no exception occurs, a Json object is returned 
        with a succes message

    Raises
    ------
    Http404
        if the requested record cannot be found
    '''
    if request.method == 'POST':
        record = get_object_or_404(MergeDirectory, pk=request.POST.get("id"))
        record.use = not record.use
        record.save()
        return JsonResponse({"success": True})


@login_required
def samplesheet(request):
    '''View for handling samplesheets. 
    Depending on the request method and the different POST
    parameters, different actions can be performed. 
    This function is meant for usage with Ajax

    Parameters
    ----------
    request : WSGIRequest

    Returns
    -------
    JsonResponse
        If no exception occurs, a Json object is returned 
        with a succes message, and required meta information.

    Raises
    ------
    Http404
        if the requested record cannot be found
    '''
    fcid = request.POST.get("fcid")
    if fcid:
        try:
            get_object_or_404(FlowCell, volgnummer=fcid)\
                .write_samplesheet()
            return JsonResponse({"success": True})
        except Exception as err:
            return JsonResponse({"success": False, "msg": str(err)})

    record = get_object_or_404(FlowCell, volgnummer=request.POST.get('id'))
    ss = record.samplesheet_file if \
        request.POST.get("generate") == 'false'\
        else record.samplesheet

    return JsonResponse(
        {
            "ss": ss,
            "type": record.instrument_type
        })


@login_required
def check_additional_samplesheets(request):
    '''View for invoking a check on the additional samplesheets.
    Meant for usage with Ajax. If there are exceptions, they are returned.

    Parameters
    ----------
    request : WSGIRequest

    Returns
    -------
    JsonResponse
        If no exception occurs, a Json object is returned 
        with a succes message. If there are exceptions in checking
        the samplesheet, there are returned as well.

    Raises
    ------
    Http404
        if the requested record cannot be found
    '''
    flowcell_ = get_object_or_404(FlowCell,
                                  volgnummer=request.POST['id'])
    exceptions = flowcell_.check_additional_samplesheets()
    return JsonResponse(
        {
            "success": not any(exceptions),
            "exceptions": exceptions
        })


def orders(request):
    '''View function for the orders overview page.
    Only the orders that have completed flowcells are returned, 
    future project are not returned. 

    Parameters
    ---------
    request : WSGIRequest

    Returns
    -------
    render
        a rendered page request, including some context and
        the paginated list of requested records.
    '''
    records = EasyTrackOrder.objects.filter(
        opdrachtcode__in=FlowCellSample.get_completed_flowcells()
    ).exclude(dienst="Next Gen Custom").order_by("-opdrachtcode")

    return render(request, 'orders.html', {
        'records': get_paginator(request, records)
    })


@login_required
def revoke(request):
    '''View for revoking a running QueueRecord from the Queue,
    Meant for using with Ajax requests.

    Parameters
    ----------
    request : WSGIRequest

    Returns
    -------
    JsonResponse
        If no exception occurs, a Json object is returned 
        with a succes message

    Raises
    ------
    Http404
        if the requested record cannot be found
    '''
    return JsonResponse(
        {
            "succes": get_object_or_404(
                QueueRecord, pk=request.POST['id']).revoke(request.user)
        })

@login_required
def retry(request):
    """Retry a record."""
    previous_record = get_object_or_404(QueueRecord, pk=request.POST['pk'])
    emails = previous_record.watcher_email
    
    def get_duplicated_record(record, username):
        """Get a shallow clone of a record."""
        command = f"{record.command} -resume" if "nextflow" in record.command else record.command
        data = {
            "command": command,
            "priority": record.priority,
            "required_cpus": record.required_cpus,
            "started_by": username,
            "watcher_email": record.watcher_email
        }
        return QueueRecord.objects.create(**data)

    record = get_duplicated_record(previous_record, request.user.get_full_name())
    async_code = excecute_system_commmand.delay(
        record_id=record.pk,
        command=record.command,
        priority=record.priority,
        required_cpus=record.required_cpus
    )
    record.async_code = async_code
    record.save()

    return JsonResponse({"success": "Succesfully retried from previous record."})
