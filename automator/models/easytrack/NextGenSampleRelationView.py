from django.db import models
from .EasyTrackModel import EasyTrackModel

class NextGenSampleRelationView(EasyTrackModel):

    nextgensampleid = models.OneToOneField('NextGenSample',
                                           db_column='NextGenSampleID',
                                           primary_key=True,
                                           on_delete=models.DO_NOTHING)
    nextgenlibraryid = models.IntegerField(db_column='NextGenLibraryID',
                                           blank=True,
                                           null=True,
                                           unique=True)



    class Meta:
        managed = False
        db_table = 'vwNextGenSampleRelationView'
