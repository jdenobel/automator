import sys
import os
from itertools import chain
from datetime import datetime

from glob import glob
from io import StringIO
import pandas as pd
import xmltodict
from django.conf import settings
from django.db import models
from django.utils.functional import cached_property
from django.contrib.auth.models import User

from pipelinelib.utils.file import Bcl2FastqSampleSheet, F088, F089, Nextera

from .EasyTrackModel import EasyTrackModel

from ..PlateToExcel import PlateToExcel
from ..DemultiplexingRecord import DemultiplexingRecord
from ..MergeDirectory import MergeDirectory


STATIC =  "/mnt/nfs/pipelines/pipelinelib/utils/file/static"
DEFAULT_INDEX_SET_FILE = os.path.join(STATIC, "indexSets.txt")

TEMPLATE = """\
[Header]
IEMFileVersion,4
Investigator Name,AUTO
Experiment Name,{illuminaid}
Date,{date}
Workflow,GenerateFASTQ
Application,FASTQ Only
Assay,Nextera XT
Instrument Type,{instrument_type}
Description
Chemistry,Amplicon
[Reads]
{n_reads}
[Settings]
[Data]
"""
HEADER = """
FCID,Lane,Sample_ID,SampleRef,\
Index,Index2,Description,Control,\
Recipe,Operator,Sample_Project,\
Filter,Code,PrepType
"""
ROW = "{illuminaid},{laan},{naam}_{code},,{i1},{i2},\
{code}{herhaling},N,{recipe},{contact_p},{project_id},\
{filter},{code},{type_prep}\n"

PACBIO_HEADER = """
FCID,Lane,SampleID,SampleRef,Index,\
Description,Control,Recipe,Operator,SampleProject,PrepType
"""
PACBIO_ROW = "{illuminaid},{laan},{naam}_{code},noref,{index},\
{code}{herhaling},N,NA,{contact_p},{project_id},{type_prep}\n"

ILLUMINA_TYPES = ['HF', 'H3', 'MS', 'HS', 'N4', 'N2', "S4", "S3", "S2", "S1", 'SP']


class FlowCell(EasyTrackModel):
    volgnummer = models.AutoField(db_column='Volgnummer', primary_key=True)
    illuminaid = models.CharField(db_column='IlluminaID', max_length=50, blank=True, null=True)
    typesequencer = models.CharField(db_column='TypeSequencer', max_length=2, blank=True, null=True)
    sequencernaam = models.CharField(db_column='Sequencernaam', max_length=50, blank=True, null=True)
    typerun = models.CharField(db_column='TypeRun', max_length=20, blank=True, null=True)
    runlengte = models.IntegerField(db_column='RunLengte', blank=True, null=True)
    status = models.CharField(db_column='Status', max_length=50, blank=True, null=True)
    invoerdatum = models.DateTimeField(db_column='Invoerdatum', blank=True, null=True)
    rundatum = models.DateTimeField(db_column='Rundatum', blank=True, null=True)
    opmerkingen = models.TextField(db_column='Opmerkingen', blank=True, null=True)


    @cached_property
    def illuminaid_nice(self):
        if self.illuminaid:
            if self.is_illumina:
                return self.illuminaid.split("_")[0]
            return self.illuminaid

    @property
    def instrument_type(self):
        if self.typesequencer in ["S4", "S3", "S2", "S1", "SP"]:
            return "NovaSeq"
        return self.sequencernaam

    @staticmethod
    def get_illumina_flowcells():
        return FlowCell.objects.filter(typesequencer__in=ILLUMINA_TYPES)

    @property
    def is_illumina(self):
        return self.typesequencer in ILLUMINA_TYPES

    @property
    def is_pacbio(self):
        return self.instrument_type == 'PacBio'

    @property
    def is_ont(self):
        return self.instrument_type == 'MinIon01'

    @property
    def projects(self):
        # including samples in pools =)
        return list({s.project for s in chain(*[s.pool_samples or [s] for s in self.samples.all()])})


    @property
    def samplesheet(self):
        if not (hasattr(self, '_samplesheet') and self._samplesheet):
            self.generate_samplesheet()
        return self._samplesheet


    @property
    def parser(self):
        if not (hasattr(self, '_parser') and self._parser):
            # fix so that the parser will work
            data = (self.samplesheet if not self.is_pacbio
                else "[Data]\n" + self.samplesheet)
            filename = None
            if self.data_folder and self.samplesheet_file_path:
                filename = os.path.join(self.data_folder,
                                        self.samplesheet_file_path)

            self._parser = Bcl2FastqSampleSheet(
                filename if filename else None,
                StringIO(data)
            )
        return self._parser

    @property
    def unseen_plates(self):
        try:
            if len(self.parser.unseen_plates) != 0:
                return self.parser.unseen_plates[
                        'Sample_ID'].values.tolist()
        except:
            return []


    @property
    def linked_plates(self):
        return PlateToExcel.objects.filter(
                plate_id__in=self.unseen_plates or []
            )

    @property
    def unchecked_plates(self):
        return self.linked_plates.filter(checked=False)

    def check_additional_samplesheets(self):
        exceptions = []
        for plate in self.unchecked_plates:
            try:
                if plate.sheet_type == 'F088':
                    self.parser.update_data(
                        F088(plate.target_path, plate_id=plate.plate_id)
                    )
                elif plate.sheet_type == 'F089':
                    self.parser.update_data(
                        F089(
                            plate.target_path,
                            plate_id=plate.plate_id,
                            index_set_id=plate.index_tag_key
                        )
                    )
                else:
                    self.parser.update_data(
                        Nextera(plate.target_path, plate_id=plate.plate_id, custom_tags=None)
                    )
                plate.checked = True
                plate.save()
            except Exception as err:
                exceptions.append(
                    (plate.name, repr(err),)
                )

        return exceptions


    @property
    def can_be_automated(self):
        if self.is_illumina:
            return bool(self.data_folder) \
                and (not bool(self.unseen_plates_repr)) \
                and (not self.is_demultiplexed) \
                and (not self.unchecked_plates.exists())
        return bool(self.data_folder)\
            and self.is_run_completed\
            and (not self.is_demultiplexed)

    @property
    def unseen_plates_repr(self):
        if self.unseen_plates:
            return  [
                x for x in self.unseen_plates
                if not self.linked_plates.filter(plate_id=x).first()
            ]
        return []


    @property
    def samplesheet_file_path(self):
        if self.data_folder:
            return (
                os.path.join(self.data_folder,
                    f"SampleSheetBcl2Fastq2_{self.illuminaid}.csv")
                if self.is_illumina else os.path.join(
                    self.data_folder,
                    f"SampleSheet_{self.illuminaid}.csv"
                )
            )
        return ''

    def samplesheet_file_exists(self):
        return os.path.isfile(self.samplesheet_file_path)

    @property
    def samplesheet_file(self):
        if os.path.isfile(self.samplesheet_file_path):
            with open(self.samplesheet_file_path, "r") as handle:
                return handle.read()

    def write_samplesheet(self, directory=''):
        if os.path.exists(directory):
            file_path = os.path.join(
                directory, 'SampleSheetBcl2Fastq.csv')
        else:
            file_path = self.samplesheet_file_path
        with open(file_path, "w+") as handle:
            handle.write(self.samplesheet)


    def user_check(self, user=None):
        if user != None:
            return user
        return User.objects.filter(settings__isnull=False).first()

    def generate_tickets(self, user=None):
        return self.user_check(user).settings.jira_service.generate_tickets(self)

    def get_tickets(self, user=None):
        return self.user_check(user).settings.jira_service.get_tickets(self)

    def update_tickets_to_status(self, status, user=None, tickets=None):
        tickets = tickets if tickets else self.get_tickets(user)
        user = self.user_check(user)
        return [
            user.settings.jira_service.set_status_for_ticket(
                ticket, status
            )
            for ticket in tickets
        ]


    @property
    def merge_dirs(self):
        merge_dirs = []
        for project in self.parser.projects:
            root = os.path.join(
                settings.DATA_ROOT,
                f"{project['Sample_Project']}_{project['Operator'].replace(' ', '_')}")
            directory = os.path.join(root,"raw_sequences")

            if os.path.isdir(root) and os.path.isdir(directory):
                for path in os.listdir(directory):
                    full_path = os.path.join(directory, path)
                    if os.path.isdir(full_path)\
                        and "awaitingadditionaldata" in str(path).lower():
                        merge_dir, _ = MergeDirectory.objects.get_or_create(
                            project_id=project['Sample_Project'],
                            path=full_path, flowcell_id=self.pk
                        )
                        merge_dirs.append(merge_dir)
        return merge_dirs

    def format_row_illumina(self, sdata, s):
        return ROW.format(
            **dict(
                sdata,
                i1=s.tags[0],
                i2=s.tags[1],
                recipe="no-filt" if s.project.is_fg  else "NA",
                filter=s.datainmb
        ))

    def format_row_pacbio(self, sdata, s):
        return PACBIO_ROW.format(
            **dict(
                sdata,                
                index=s.single_index
        ))


    def generate_samplesheet(self):
        if self.is_illumina:
            self._samplesheet = TEMPLATE.format(
                date=(self.rundatum or datetime.now()).strftime("%y%m%d"),
                illuminaid=self.illuminaid,
                n_reads=(self.runlengte if self.typerun != "PE"\
                else f"{self.runlengte}\n{self.runlengte}"),
                instrument_type=self.instrument_type
            ) + HEADER

        elif self.is_pacbio:
            self._samplesheet = PACBIO_HEADER
        else:
            self._samplesheet = 'Unknown Samplesheet Type {}'.format(
                self.instrument_type
            )

        samples = chain(*[s.pool_samples or [s] for s in self.samples.all()])
        for s in samples:
            sdata = dict(s.__dict__,
                naam=s.naam.replace(",", ""),
                type_prep=s.type_prep,
                illuminaid=self.illuminaid,
                contact_p=s.project.contact_person,
                herhaling=s.herhaling if s.herhaling else '',
            )
            try:
                if self.is_illumina:
                    self._samplesheet += self.format_row_illumina(sdata, s)
                elif self.is_pacbio:
                    self._samplesheet += self.format_row_pacbio(sdata, s)
            except Exception as e:
                print(e)


    @property
    def data_root(self):
        if self.instrument_type == 'NovaSeq':
            return settings.NOVASEQ_DATA_ROOT
        elif self.is_illumina:
            return settings.ILLUMINA_DATA_ROOT
        elif self.is_pacbio:
            return settings.PACBIO_ROOT
        elif self.is_ont:
            return settings.ONT_DATA_ROOT
        return ''


    @cached_property
    def data_folder(self):
        if self.rundatum:
            if self.is_illumina:
                datestring = self.rundatum.strftime("%y%m%d")
                for folder in os.listdir(self.data_root):
                    joined = os.path.join(self.data_root, folder)
                    if not os.path.isdir(joined): continue
                    if folder.startswith(datestring)\
                    and folder.endswith(self.illuminaid_nice):
                        return joined
            elif self.is_pacbio:
                xmls = glob(
                    os.path.join(self.data_root,
                    f"*{self.rundatum.strftime('%Y')}*",'*', "*subreadset.xml")
                )
                for xml in xmls:
                    with open(xml) as fd:
                        data = xmltodict.parse(fd.read())
                        name = data['pbds:SubreadSet']\
                            .get("@Name").split("-")[0].strip()
                        if name == self.illuminaid:
                            return os.path.dirname(xml)

            elif self.is_ont:
                matches = list(filter(os.path.isdir, glob(
                    os.path.join(self.data_root,"*", "*", "*{}*".format(self.illuminaid))
                )))
                if any(matches):
                    return os.path.dirname(matches[0])
        return ''

    @cached_property
    def folder_content(self):
        if self.data_folder:
            return sorted(map(
                lambda x: (os.path.isdir(os.path.join(self.data_folder, x)), x,), 
                os.listdir(self.data_folder)
            ), key=lambda x:x[0], reverse=True)
        return []

    @property
    def html_files(self):
        def mapper(filepath):
            filename = os.path.basename(filepath)
            return {
                "path": filepath,
                "name": filename.split(".")[0],
                "filename": filename
            }
        if self.is_ont:
            return map(mapper, glob(os.path.join(self.data_folder, "*.html")))
        if self.is_illumina:
            html_loc = os.path.join(self.data_folder, self.default_output_folder,
                        "*", "Reports*", "Reports/html",
                        "*", "all/all/all")
       
            return map(mapper, glob(os.path.join(html_loc, "*html")))

    @property
    def is_run_completed(self):
        if self.data_folder:
            if self.is_illumina:
                return os.path.isfile(
                    os.path.join(self.data_folder, "RTAComplete.txt"))
            if self.is_pacbio:
                return os.path.isfile(
                    os.path.join(self.data_folder, ".downloaddone"))

        return False

    @property
    def demultiplexing_records(self):
        return DemultiplexingRecord.objects.filter(flowcell_id=self.pk)

    @property
    def default_output_folder(self):
        if self.data_folder:
            if self.is_illumina:
                return (
                    os.path.join(self.data_folder,
                        "Data/Intensities/BaseCalls/SequenceData/"
                    )
                )
            if self.is_pacbio:
                result_dirs = glob(
                    os.path.join(self.data_folder, "sequel_project*"))
                if result_dirs:
                    return result_dirs[0] 
            if self.is_ont:
                pass
        return ''      
    
    @property
    def is_demultiplexed(self):
        return self.demultiplexing_records.exists() or (
            self.default_output_folder and os.path.isdir(
                self.default_output_folder))

    @property
    def exceptions(self):
        try:
            self.parser
        except Exception as err:
            return str(err)
        return None

    @property
    def xlsx_files(self):
        if self.data_folder and len(self.parser.unseen_plates) != 0:
            files = []
            for project_id in self.parser.unseen_plates[
                    'Sample_Project'].unique().tolist():
                files.extend(
                    [   [os.path.basename(x), x]
                        for x in glob(
                        os.path.join(
                            settings.I_DRIVE, str(project_id), "*xls*")
                        ) + glob( os.path.join(self.data_folder, "*xls*"))
                    ]
                )
            seen = set([os.path.basename(x.target_path) for x in self.linked_plates])
            files = list(filter(lambda f:f[0] not in seen, files))

            return files
        return []

    @staticmethod
    def get_sequencing_flowcells():
        return FlowCell.objects.filter(status="Sequencing")


    class Meta:
        db_table = 'tblNextGenFlowcells'
        managed = False

    def __str__(self):
        return f"[{self.volgnummer}] {self.illuminaid}"
