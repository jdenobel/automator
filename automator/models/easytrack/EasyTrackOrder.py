from csv import DictReader
import os
from datetime import datetime, timedelta
from glob import glob

from django.db import models
from django.db.models import Q
from django.conf import settings
from django.utils.functional import cached_property

from .EasyTrackModel import EasyTrackModel
from .OrderArtikel import OrderArtikel
from .FlowCellSample import FlowCellSample
from ..ETToken import ETToken


MIN_P_SAMPLES_W_ARTIKEL = .8


def get_product_codes(filename='product_codes.csv'):
    """Read from csv and create a list with the product codes."""
    product_codes = []
    # expect the csv to be located in the same directory as this file.
    filename = os.path.join(os.path.dirname(__file__), filename)
    with open(filename, mode='r') as csv_file:
        # TODO: could this raise exception if duplicate keys in file?
        csv_reader = DictReader(csv_file, delimiter=',')
        for row in csv_reader:
            number = row['number'].strip()
            if number:
                product_codes.append(number)
    return product_codes


class EasyTrackOrder(EasyTrackModel):
    opdrachtcode = models.IntegerField(
        db_column='Opdrachtcode', unique=True, primary_key=True)
    # Field name made lowercase.
    leverdatum = models.DateTimeField(
        db_column='Leverdatum', blank=True, null=True)
    # Field name made lowercase.
    klantnummer = models.ForeignKey(
        'EasyTrackUser', db_column='Klantnummer', blank=True, null=True, on_delete=models.DO_NOTHING)
    dienst = models.CharField(max_length=50, blank=True, null=True)
    verwachte_einddatum = models.DateTimeField(
        db_column='Verwachte einddatum', blank=True, null=True)
    projecteigenaar = models.CharField(max_length=50, blank=True, null=True)
    # Field name made lowercase.
    orderdatum = models.DateTimeField(
        db_column='Orderdatum', blank=True, null=True)

    @property
    def sample_max_levertijd(self):
        objects = self.nextgensamples.aggregate(models.Max('leverdatum'))
        two_weeks = datetime.now() + timedelta(weeks=2)  # Default two weeks from now
        end_date = objects.get("leverdatum__max", two_weeks)
        return end_date.date() if end_date else two_weeks.date()

    @property
    def is_premium(self):
        if 'premium' in self.dienst.lower():
            return True
        return self.all_artikelen.filter(produktnummer__contains='PREMIUM').exists()

    @property
    def is_custom(self):
        if 'custom' in self.dienst.lower():
            return True
        return self.all_artikelen.filter(produktnummer__contains='CUSTOM').exists()

    @property
    def artikelen(self):
        tokens = ETToken.objects.exclude(pipeline=None)
        return OrderArtikel.objects.filter(
            opdrachtcode=self.opdrachtcode,
            # filter by valid codes
            produktnummer__in=list(tokens.values_list('tag', flat=True)),
            # filter by number of samples per artikel
        ).exclude(
            # If n_sample_filter is True, use this filter (.8 percent of samples must have analysis)
            produktnummer__in=list(
                tokens.filter(pipeline__n_sample_filter=True).values_list('tag', flat=True)),
            hoeveelheid__lt=int(MIN_P_SAMPLES_W_ARTIKEL*len(self.samples))
        )

    @property
    def all_artikelen(self):
        return OrderArtikel.objects.filter(
            opdrachtcode=self.opdrachtcode,
        )

    @property
    def is_rivm(self):
        return self.company_name and self.company_name.startswith("RIVM") \
            and self.contact_person in ["NGS_IDS", "NGS IDS"]

    @property
    def is_fg(self):
        return self.company_name == "Future Genomics Technologies"

    @property
    def bioinf_artikelen(self):
        return OrderArtikel.objects.filter(
            opdrachtcode=self.opdrachtcode).filter(
                produktnummer__in=get_product_codes()
        )

    def get_customer(self):
        try:
            return self.klantnummer
        except:
            return None

    @property
    def contact_person(self):
        customer = self.get_customer()
        if customer:
            return customer.contactpersoon.strip().replace(" ", "_")

    @property
    def company_name(self):
        customer = self.get_customer()
        if customer:
            return customer.klantennaam

    @property
    def samples(self):
        return FlowCellSample.objects.filter(
            project=self.opdrachtcode
        )

    @property
    def n_samples(self):
        return len(self.samples)

    @property
    def can_be_used(self):
        try:
            if self.artikelen:  # and not self.nextgen.is_custom:
                return True
        except Exception as error:
            print(error)
        return False

    def __repr__(self):
        return f"Order: [{str(self.pk)}]"

    @property
    def data_location(self):
        dirs = glob(
            os.path.join(
                settings.DATA_ROOT, f'{self.opdrachtcode}*'
            )
        )
        if any(dirs):
            return dirs[0]
        
        return os.path.join(
            settings.DATA_ROOT,
            f'{self.opdrachtcode}_{self.contact_person}'
        )

    class Meta:
        db_table = 'tblOrders'
        managed = False
