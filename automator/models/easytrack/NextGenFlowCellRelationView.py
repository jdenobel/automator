from django.db import models


class Vwnextgenflowsamplerelationview(models.Model):
    # Field name made lowercase.
    code = models.IntegerField(db_column='Code', blank=True, null=True)
    # Field name made lowercase.
    herhaling = models.CharField(
        db_column='Herhaling', max_length=1, blank=True, null=True)
    # Field name made lowercase.
    nextgensampleid = models.IntegerField(db_column='NextGenSampleID')
    # Field name made lowercase.
    nextgenflowcellsampleid = models.IntegerField(
        db_column='NextGenFlowcellSampleID')

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'vwNextGenFlowSampleRelationView'
