from django.db import models
from django.conf import settings
from .EasyTrackModel import EasyTrackModel


class NextGenSample(EasyTrackModel):
    """"""
    if settings.ET_POOLS_SUPPORTED:
        nextgensampleid = models.AutoField(db_column='NextGenSampleID', primary_key=True)
        code = models.IntegerField(db_column='Code')
    else:
        code = models.IntegerField(db_column='Code', primary_key=True)

    naam = models.CharField(db_column='Naam', max_length=50, blank=True, null=True)
    project = models.ForeignKey('EasyTrackOrder',
                                to_field='opdrachtcode',
                                db_column='Project',
                                on_delete=models.DO_NOTHING,
                                related_name='nextgensamples')
    samplestatus = models.CharField(db_column='Samplestatus', max_length=50, blank=True, null=True)
    species = models.TextField(db_column='Species', blank=True, null=True)
    datainmb = models.IntegerField(db_column='DataInMb', blank=True, null=True)
    herhaling = models.CharField(db_column='Herhaling', max_length=1, blank=True, null=True)
    indextag = models.IntegerField(db_column='IndexTag', blank=True, null=True)
    type_prep = models.CharField(db_column='TypePrep', max_length=50, blank=True, null=True)

    class Meta:
        db_table = 'tblNextGenSamples'
        managed = False

    def __str__(self):
        return f"{self.__class__.__name__}: {self.code}"
