import json

from .EasyTrackModel import EasyTrackModel
from django.db import models


class NextGenProject(EasyTrackModel):
    projectcode = models.OneToOneField('EasyTrackOrder', db_column='Projectcode',
                                       on_delete=models.DO_NOTHING, related_name="nextgen", primary_key=True)
    datatransfer = models.CharField(
        db_column='DataTransfer', max_length=50, blank=True, null=True)
    opmerkingen = models.TextField(
        db_column='Opmerkingen', blank=True, null=True)
    uitvoerder = models.IntegerField(
        db_column='Uitvoerder', blank=True, null=True)
    bioinformaticaopmerkingen = models.TextField(
        db_column='BioinformaticaOpmerkingen', blank=True, null=True)
    custom = models.BooleanField()
    custom_options = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'tblNextGenProject'
        managed = False

    @property
    def is_custom(self):
        return self.custom or self.parsed_comments.get("custom")

    def __map_key_value(self, pair, sep="="):
        splitted = pair.split(sep)
        if len(splitted) == 2:
            return splitted[0], splitted[1]
        return splitted[0], None

    @property
    def extra_options(self):
        parsed_comments = self.parsed_comments
        parsed_comments.pop("custom", None)
        if self.custom_options:
            parsed_comments.update(**dict(
                self.__map_key_value(x)
                for x in self.custom_options.split(";")
            ))
        return parsed_comments

    @property
    def parsed_comments(self):
        # this is obsolete
        if self.bioinformaticaopmerkingen:
            for line in self.bioinformaticaopmerkingen.splitlines():
                if line.startswith("AUTOMATOR=") or line.startswith("AUTOMATER="):
                    json_string = line.split("=")[-1].replace("'", '"')
                    return json.loads(json_string)
        return {}
