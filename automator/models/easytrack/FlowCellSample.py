import os

from django.db import models, utils
from django.conf import settings
from .EasyTrackModel import EasyTrackModel
from .NextGenSample import NextGenSample
from .IndexTag import IndexTag


class FlowCellSample(EasyTrackModel):
    if settings.ET_POOLS_SUPPORTED:
        sample_id = models.IntegerField(
            db_column='NextGenFlowcellSampleID', primary_key=True)

        code = models.IntegerField(db_column='Code')
    else:
        code = models.IntegerField(db_column='Code', primary_key=True)

    volgnummer = models.ForeignKey('FlowCell', db_column='Volgnummer', blank=True,
                                   null=True, on_delete=models.DO_NOTHING, related_name="samples")
    project = models.ForeignKey('EasyTrackOrder', db_column='Project',
                                blank=True, null=True, on_delete=models.DO_NOTHING)
    naam = models.CharField(
        db_column='Naam', max_length=50, blank=True, null=True)
    laan = models.IntegerField(db_column='Laan', blank=True, null=True)
    typerun = models.CharField(
        db_column='TypeRun', max_length=6, blank=True, null=True)
    herhaling = models.CharField(
        db_column='Herhaling', max_length=1, blank=True, null=True)
    indextag = models.IntegerField(db_column='IndexTag', blank=True, null=True)
    type_prep_db = models.CharField(
        db_column='TypePrep', max_length=50, blank=True, null=True)
    datainmb = models.IntegerField(db_column='DataInMb', blank=True, null=True)
    datainmbklant = models.IntegerField(
        db_column='DataInMbKlant', blank=True, null=True)
    percentagevanlaan = models.IntegerField(
        db_column='PercentageVanLaan', blank=True, null=True)
    primer = models.CharField(
        db_column='Primer', max_length=50, blank=True, null=True)
    runlengte = models.CharField(
        db_column='Runlengte', max_length=20, blank=True, null=True)
    samplestatus = models.CharField(
        db_column='Samplestatus', max_length=50, blank=True, null=True)
    opmerkingen = models.TextField(
        db_column='Opmerkingen', blank=True, null=True)

    @property
    def tag(self):
        return IndexTag.objects.filter(volgnummer=self.indextag).first()

    @property
    def nextgensample(self):
        return NextGenSample.objects.filter(code=self.code, herhaling=self.herhaling).first()

    @property
    def type_prep(self):
        if self.nextgensample:
            return self.nextgensample.type_prep or self.type_prep_db or "NA"
        return self.type_prep_db or "NA"

    @property
    def pool_samples(self):
        if not settings.ET_POOLS_SUPPORTED:
            return

        try:
            if self.nextgensample and hasattr(self.nextgensample, 'pool'):
                pool = self.nextgensample.pool
                return [
                    FlowCellSample(
                        laan=self.laan,
                        volgnummer=self.volgnummer,
                        code=s.code,
                        project=s.project,
                        naam=s.naam,
                        herhaling=s.herhaling,
                        indextag=s.indextag,
                        datainmb=s.datainmb

                    )
                    for s in pool.samples
                ]
        except utils.ProgrammingError:
            pass

    @property
    def single_index(self):
        if self.indextag in [0, 100000, 666666]:
            return self.indextag
        if not self.tag.seq1:
            return 0
        return self.tag.seq1 + self.tag.seq2

    @property
    def tags(self):
        try:
            if self.tag.volgnummer == 0:
                return "", ""
            joined = self.tag.seq1 + (self.tag.seq2 if self.tag.seq2 else '')
            len_ = len(joined)
            if len_ in [16, 20]:
                return joined[:len_//2], joined[len_//2:]
            return joined[:6], ''
        except:
            return '', ''

    @staticmethod
    def get_completed_flowcells():
        return FlowCellSample.objects.select_related(
            "volgnummer", "project"
        ).filter(
            volgnummer__status__in=['Vrijgegeven', "Run klaar"],
            project__leverdatum__isnull=True
        ).values_list("project", flat=True).distinct()

    def get_data_root(self, artikel_root):
        root = os.path.join(
            self.project.data_location,
            artikel_root
        )
        for name in os.listdir(root):
            joined = os.path.join(root, name)
            if not os.path.isdir(joined):
                continue
            if "awaitingadditionaldata" in str(name).lower():
                continue
            if "contaminated" in str(name).lower():
                continue
            # If starts with flowcell id
            if self.volgnummer.illuminaid_nice and self.volgnummer.illuminaid_nice in str(name):
                return joined
            if self.naam in name:
                return joined

        raise FileNotFoundError(
            (
                f"Directory of FlowCell [{self.volgnummer.illuminaid_nice}] "
                f"not found at {root} for sample [{self.code}]"
            )
        )

    def get_input_files(self, extension, file_pattern, artikel_root, number_of_files=1):
        files = []
        data_root = self.get_data_root(artikel_root)

        for name in os.listdir(data_root):
            joined = os.path.join(data_root, name)
            underscore_splitted = list(reversed(name.split("_")))

            if underscore_splitted[0] == file_pattern and (
                underscore_splitted[1] == str(self.code) or
                underscore_splitted[1] == str(self.naam)
            ):
                files.append(joined)

            elif not os.path.isfile(joined)\
                    or not os.path.splitext(joined)[1] == extension\
                    or len(underscore_splitted) < 6:
                continue
            elif underscore_splitted[5].isdigit() and \
                    int(underscore_splitted[5]) == self.code:
                files.append(joined)

        if len(files) == number_of_files:
            return files

        raise FileNotFoundError(
            (
                f"A different number of files were found for "
                f"{self.code} at {data_root} than were expected\n"
                f"(found: {len(files)} Expected: {number_of_files})"
            )
        )

    class Meta:
        managed = False
        db_table = 'tblNextGenFlowcellSamples'
        unique_together = (
            "code",
            "volgnummer"
        )

    def __str__(self):
        return f"[{self.code}]"  # [{self.project.pk}]"
