import xmltodict

from django.db import models

from core.utils import str_to_type
from .EasyTrackModel import EasyTrackManager, EasyTrackModel


class FlowCellStats(EasyTrackModel):
    ## SAV one
    objects = EasyTrackManager("EasyTrack")
    numeric_fields = [
          "cyclessequencedread2", "laan",
          "tiles", "cyclessequencedread1",
    
    ]
    class Meta:
        db_table = 'tblNextGenFlowcellstatistieken'   
        managed = False
        

    illuminaid = models.CharField(db_column='IlluminaID', max_length=50,  primary_key=True)  # Field name made lowercase.
    density = models.CharField(db_column='Density', max_length=50, blank=True, null=True)  # Field name made lowercase.
    clusterspf = models.CharField(db_column='ClustersPF', max_length=50, blank=True, null=True)  # Field name made lowercase.
    reads = models.CharField(db_column='Reads', max_length=50, blank=True, null=True)  # Field name made lowercase.
    readspf = models.CharField(db_column='ReadsPF', max_length=50, blank=True, null=True)  # Field name made lowercase.
    percq30read1 = models.CharField(db_column='PercQ30Read1', max_length=50, blank=True, null=True)  # Field name made lowercase.
    alignedperc = models.CharField(db_column='AlignedPerc', max_length=50, blank=True, null=True)  # Field name made lowercase.
    errorrateread1 = models.CharField(db_column='ErrorRateRead1', max_length=50, blank=True, null=True)  # Field name made lowercase.
    percq30read2 = models.CharField(db_column='PercQ30Read2', max_length=50, blank=True, null=True)  # Field name made lowercase.
    errorrateread2 = models.CharField(db_column='ErrorRateRead2', max_length=50, blank=True, null=True)  # Field name made lowercase   

    cyclessequencedread2 = models.IntegerField(db_column='CyclesSequencedRead2', blank=True, null=True)  # Field name made lowercase.    
    laan = models.IntegerField(db_column='Laan', blank=True, null=True)  # Field name made lowercase.
    tiles = models.IntegerField(db_column='Tiles', blank=True, null=True)  # Field name made lowercase.
    cyclessequencedread1 = models.IntegerField(db_column='CyclesSequencedRead1', blank=True, null=True)  # Field name made lowercase.
    

    @staticmethod
    def read_summary_file(path):   
        with open(path, 'r') as handle:
            data = xmltodict.parse(handle.read()).get("RunSummaryReport")
            for lane in data.get("laneTag"):
                lane['IlluminaID'] = data['IlluminaID']
                record, created = FlowCellStats.objects.get_or_create(**{
                    k.lower(): str_to_type(v) if k.lower() in FlowCellStats.numeric_fields else v
                    for k, v in lane.items() })
      

