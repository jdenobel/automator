import os
from glob import glob
from django.db import models

from .EasyTrackModel import EasyTrackModel
from automator.models.ExecutionRecord import ExecutionRecord
from ..ETToken import ETToken


class OrderArtikel(EasyTrackModel):
    # Field name made lowercase.
    opdrachtcode = models.ForeignKey(
        "EasyTrackOrder", db_column='Opdrachtcode', on_delete=models.DO_NOTHING)
    # Field name made lowercase.
    produktnummer = models.CharField(db_column='Produktnummer', max_length=20)
    # Field name made lowercase.
    produktomschrijving = models.CharField(
        db_column='Produktomschrijving', max_length=50, blank=True, null=True)
    # Field name made lowercase.
    volgnummer = models.IntegerField(
        db_column='Volgnummer', blank=True, null=True)
    # Field name made lowercase.
    hoeveelheid = models.IntegerField(
        db_column='Hoeveelheid', blank=True, null=True)
    factuurnummer = models.CharField(
        db_column='Factuurnummer', max_length=50, primary_key=True)

    @property
    def pipeline(self):
        et_token = ETToken.objects.get(tag=self.produktnummer)
        if et_token:
            return et_token.pipeline

    def get_correct_dir(self):
        base = os.path.join(
            self.opdrachtcode.data_location,
            self.pipeline.output_folder
        )
        base_names = list(map(os.path.basename, glob(base + "*")))
        if len(base_names) == 1:
            return os.path.join(self.opdrachtcode.data_location, base_names[0])
        elif len(base_names) > 1:
            return os.path.join(self.opdrachtcode.data_location,
                                min(base_names, key=len))
        return base

    def has_output_folder(self, sample):
        base = self.get_correct_dir()
        if not self.pipeline.per_sample:
            return os.path.isdir(base)

        output_folder_w_id = os.path.join(base, str(sample.pk))
        output_folder_w_name = os.path.join(base, str(sample.naam))
        output_folder_w_name_underscore = os.path.join(
            base, str(sample.naam.replace("-", "_")))

        return (os.path.isdir(output_folder_w_id) or
                os.path.isdir(output_folder_w_name) or
                os.path.isdir(output_folder_w_name_underscore))

    def is_performed(self, sample):
        return self.has_output_folder(sample) or ExecutionRecord.objects.filter(
            samples__sample_id=sample.pk,
            pipeline__tokens__tag__contains=self.produktnummer
        ).first() != None

    def __str__(self):
        return f"{self.opdrachtcode}: {self.produktnummer} {self.hoeveelheid}x"

    class Meta:
        db_table = 'tblOrderArtikelen'
        managed = False
        unique_together = (
            "opdrachtcode",
            "volgnummer",
            "produktnummer"
        )
