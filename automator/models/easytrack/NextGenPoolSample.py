from django.db import models
from .EasyTrackModel import EasyTrackModel


class NextGenPoolSample(EasyTrackModel):
    """"""
    poolsampleid = models.AutoField(db_column='PoolSampleId', primary_key=True)
    sample = models.ForeignKey('NextGenSampleRelationView',
                                  db_column='SampleID',
                                  to_field='nextgenlibraryid',
                                  related_name="pool_samples",
                                  on_delete=models.DO_NOTHING)
    pool = models.ForeignKey('NextGenPool',
                             db_column='PoolID',
                             on_delete=models.DO_NOTHING,
                             related_name='pool_samples')

    class Meta:
        managed = False
        db_table = 'tblNextGenPoolSample'
        unique_together = (('sample', 'pool'),)
