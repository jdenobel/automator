from django.db import models
from django.conf import settings
from .EasyTrackModel import EasyTrackModel


class NextGenPool(EasyTrackModel):
    """"""
    poolid = models.AutoField(db_column='PoolID', primary_key=True)

    poolsample = models.OneToOneField(
        "NextGenSample",
        db_column='PoolSampleID',
        on_delete=models.DO_NOTHING,
        related_name="pool"
    )

    creationdate = models.DateTimeField(db_column='CreationDate')
    userid = models.IntegerField(db_column='UserId')
    pooltype = models.CharField(db_column='PoolType', max_length=10)

    sample_views = models.ManyToManyField('NextGenSampleRelationView', through='NextGenPoolSample')

    @property
    def samples(self):
        """"""
        return [s.nextgensampleid for s in self.sample_views.all()]

    class Meta:
        managed = False
        db_table = 'tblNextgenPools'
