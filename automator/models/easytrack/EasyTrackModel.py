from django.db import models


class EasyTrackManager(models.Manager):
    def __init__(self, db_name="EasyTrack", *args, **kwargs):
        self.db_name = db_name
        super().__init__(*args, **kwargs)

    def get_queryset(self):
        return super().get_queryset().using(self.db_name)


class EasyTrackModel(models.Model):
    objects = EasyTrackManager()

    class Meta:
        managed = False
        abstract = True
