from django.db import models

from .EasyTrackModel import EasyTrackModel

class IndexTag(EasyTrackModel):
    volgnummer = models.IntegerField(db_column='Volgnummer', primary_key=True)
    seq1 = models.CharField(db_column='Sequentie', max_length=50, blank=True, null=True)  
    seq2 = models.CharField(db_column='Sequentie2', max_length=50, blank=True, null=True) 

    class Meta:
        db_table = 'tblNextGenIndexTags'
        managed = False

    
    