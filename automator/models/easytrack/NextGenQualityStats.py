import xmltodict

from django.db import models, transaction

from core.utils import str_to_type
from .EasyTrackModel import EasyTrackManager,EasyTrackModel

from pprint import pprint 

class NextGenQualityStats(EasyTrackModel):
    numeric_fields = [
        "laan",
        "project"
    ]
    objects = EasyTrackManager("EasyTrack")

    class Meta:
        db_table = 'tblNextGenKwaliteitsanalyse'
        managed = False
        
        

    illuminaid = models.CharField(db_column='IlluminaID', max_length=50, primary_key=True)  # Field name made lowercase.
    laan = models.IntegerField(db_column='Laan', blank=True, null=True)  # Field name made lowercase.
    samplename = models.CharField(db_column='SampleName', max_length=50, blank=True, null=True)  # Field name made lowercase.
    code = models.IntegerField(db_column='Code', blank=True, null=True)  # Field name made lowercase. ForeignKey

    herhaling = models.CharField(db_column='Herhaling', max_length=1, blank=True, null=True)  # Field name made lowercase.
    averagequalfilt = models.CharField(db_column='AverageQualFilt', max_length=50, blank=True, null=True)  # Field name made lowercase.
    fastqcfails = models.CharField(db_column='FastQCFails', max_length=50, blank=True, null=True)  # Field name made lowercase.
    gccontent = models.CharField(db_column='GCContent', max_length=50, blank=True, null=True)  # Field name made lowercase.
    indextag = models.CharField(db_column='IndexTag', max_length=50, blank=True, null=True) # Field name made lowercase.
    kwaliteitsscore = models.CharField(db_column='Kwaliteitsscore', max_length=50, blank=True, null=True)  # Field name made lowercase.
    percentagepfclusters = models.CharField(db_column='PercentagePFClusters', max_length=50, blank=True, null=True)  # Field name made lowercase.
    percentageq30 = models.CharField(db_column='PercentageQ30', max_length=50, blank=True, null=True)  # Field name made lowercase.
    project = models.IntegerField(db_column='Project', blank=True, null=True)  # Field namemade lowercase.
    sampleyieldinmbfilt = models.CharField(db_column='SampleYieldInMBFilt', max_length=50, blank=True, null=True)  # Field name made lowercase.
    sampleyieldinmb = models.CharField(db_column='SampleYieldInMb', max_length=50, blank=True, null=True)  # Field name made lowercase.
    numberofreads = models.CharField(db_column='NumberOfReads', max_length=50, blank=True, null=True)  # Field name made lowercase.
    overgezet = models.BooleanField(db_column='Overgezet', blank=True, null=True)  # Field name made lowercase.

    @staticmethod
    def read_summary_file(path):
        with open(path, 'r') as handle:
            data = xmltodict.parse(handle.read()).get("flowcell")
            lane_info = data.get("LaanInfo")
            with transaction.atomic():
                for sample in lane_info.get("SampleInfo"):
                    sample['IlluminaID'] = data.get('IlluminaID') or data.get('IlluminID')
                    sample['Laan'] = lane_info.get("Laan")
                    record, created = NextGenQualityStats.objects.get_or_create(**{
                        k.lower(): str_to_type(v) if k.lower() in NextGenQualityStats.numeric_fields else v
                        for k, v in sample.items() })                
    
   



# shell: NextGenQualityStats.read_summary_file(path)