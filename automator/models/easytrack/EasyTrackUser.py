from django.db import models

from .EasyTrackModel import EasyTrackModel


class EasyTrackUser(EasyTrackModel):
    # Field name made lowercase.
    klantnummer = models.FloatField(db_column='Klantnummer', primary_key=True)
    # Field name made lowercase.
    contactpersoon = models.CharField(
        db_column='ContactPersoon', max_length=114, blank=True, null=True)
    # Field name made lowercase.
    klantennaam = models.CharField(
        db_column='Klantennaam', max_length=50, blank=True, null=True)
    # Field name made lowercase.
    aanhef = models.CharField(
        db_column='Aanhef', max_length=80, blank=True, null=True)
    extra = models.CharField(max_length=50, blank=True, null=True)

    @property
    def last_name(self):
        """Get the last name of the client

        The EasyTrack database does not store the data correctly, so using two fields 
        we can extract the last name of the client
        """
        aanhef = getattr(self, 'aanhef')
        contactpersoon = getattr(self, 'contactpersoon')

        if not aanhef:
            return contactpersoon

        common_parts = set(aanhef.split()) & set(contactpersoon.split())
        return ' '.join([w for w in contactpersoon.split() if w in common_parts])

    @property
    def ticketname(self):
        clientname = self.last_name
        if not clientname:
            clientname = super().__str__()
        if self.extra and clientname != self.extra:
            clientname += f' ({self.extra})'
        return clientname

    class Meta:
        db_table = 'tblAdressenbestand'
        managed = False
