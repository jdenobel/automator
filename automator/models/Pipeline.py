import os

from django.db import models
from django.conf import settings

DEFAULTS = {
    "denovo-assembly": {
        "executable": "python",
        "main_script": "de_novo_assembly/short-read_assembly/run_denovo_shortread_assembly.py",
        "n_input_files": 2,
        "input_file_ext": ".fastq",
        'input_file_pattern': '_R1_',
        "input_folder": "raw_sequences",
        "output_folder": "de_novo_short-read_assembly",
        "required_cpus": 6,
        "project_arg": "-p",
        "file_arg": "",
        "n_sample_filter": True,
        "option_callback": None,
    },
    "bacterial-annotation": {
        "executable": "python",
        "main_script": "genome_annotation/bacterial/genomeAnnotationBacterial.py",
        "n_input_files": 1,
        "input_file_ext": ".fna",
        'input_file_pattern': 'scaffold-sequences.fna',
        "input_folder": "de_novo_short-read_assembly",
        "output_folder": "genome_annotation",
        "required_cpus": 10,
        "project_arg": "-p",
        "file_arg": "-f",
        "n_sample_filter": True,
        "option_callback": None,
    },
    "otu-profiling": {
        "executable": "python",
        "main_script": "otu-classification/OTUClassification.py",
        "n_input_files": 2,
        "input_file_ext": ".fastq",
        'input_file_pattern': '_R1_',
        "input_folder": "raw_sequences",
        "output_folder": "otu-classification",
        "required_cpus": 8,
        "project_arg": "-p",
        "file_arg": "-f",
        "n_sample_filter": False,
        "option_callback": "otu_callback",
    }
}


class Pipeline(models.Model):
    base_name = models.CharField(max_length=200, unique=True)  # Language
    executable = models.CharField(max_length=200, null=True)  # Language
    main_script = models.CharField(max_length=1000, null=True)
    n_input_files = models.IntegerField(default=1)
    input_file_ext = models.CharField(max_length=100)
    input_file_pattern = models.CharField(max_length=500, null=True)
    required_cpus = models.IntegerField(default=1)
    project_arg = models.CharField(max_length=50, null=True)
    file_arg = models.CharField(max_length=50, null=True)
    n_sample_filter = models.BooleanField(default=True)
    option_callback = models.CharField(max_length=200, null=True)
    input_folder = models.CharField(max_length=1000)
    output_folder = models.CharField(max_length=1000, null=True)
    per_sample = models.BooleanField(default=True)

    @property
    def name(self):
        return self.base_name

    @staticmethod
    def load_initial():
        from .ETToken import OTU, ANNOTATION, DENOVO, ETToken
        for codes, pipeline in zip(
            (OTU, ANNOTATION, DENOVO,),
            ("otu-profiling", "bacterial-annotation", "denovo-assembly",)
        ):
            pipeline, _ = Pipeline.objects.get_or_create(
                base_name=pipeline,
                **DEFAULTS[pipeline]
            )
            pipeline.tokens.set(ETToken.objects.filter(tag__in=codes))
            pipeline.save()
