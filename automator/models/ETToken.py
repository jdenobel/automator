from django.db import models 

## INITIALS
DENOVO = [
    "BC-1401",
    "BC-1401-PHA",
    "BC-1401-PLM",

    # ---------- PREVIOUS PRODUCT CODES
    # "BC-1401",
    # "BC-1401-PHA",
    # "BC-1401-PLM",
    # "BC-1403",
    # "BC-1400",
    # 'BC-B-ASSEMBLY',
    # 'BC-B-ASSEMBLY-PACBIO',
    # 'BC-B-ASSEMBLY-POLISH',
    # 'BC-B-ASSEMBLY_A',
    # 'BC-B-ASSEMBLY_B' ,
]
ANNOTATION = [
    "BC-1404",
    "BC-1405",
    "BC-1406",

    # ---------- PREVIOUS PRODUCT CODES
    # "BC-1404",
    # "BC-1407",
    # 'BC-B-ANNOT-BACT',
    # 'BC-B-ANNOT_BACT_A',
    # 'BC-B-ANNOT_BACT_B',
    # 'BC-B-ANNOT_BACT_C',
    # 'BC-B-ANNOT_BACT_E',
]
OTU = [
    "BC-1409-ARC",
    "BC-1409-BAC",
    "BC-1409-FUN",
    'BC-MMZ-001',
    'BC-MMZ-002',
    
    # ---------- PREVIOUS PRODUCT CODES
    # "BC-1409",
    # "BC-1409-FUN", 
    # "BC-1409-BAC",  
    # "BC-1409-ARC",
    # "BC-B-16S-ITS_1A_A",
    # "BC-B-16S-ITS_1A_B",
    # "BC-B-16S-ITS_1A_F",
    # "BC-B-16S-ITS_1A_G",
    # "BC-B-16S-ITS_1A_H",   
    # "BC-B-PROFILING" ,   
    # "BC-B-PROFILING-EXT",
    # "BC-MMZ-002",
]

class ETToken(models.Model):
    tag = models.CharField(max_length=200, unique=True, primary_key=True)
    name = models.CharField(max_length=500, null=True)
    pipeline = models.ForeignKey(
        "automator.Pipeline", 
        related_name="tokens", 
        on_delete=models.CASCADE, 
        null=True)


    @staticmethod
    def sync_et():
        from .easytrack.OrderArtikel import OrderArtikel
        codes = OrderArtikel.objects\
                .filter(produktnummer__startswith="BC-")\
                .values_list("produktnummer", flat=True)\
                .distinct()
        for code in codes:
            ETToken.objects.get_or_create(tag=code)    
        
     

    def __str__(self):
        return self.tag
        
        


