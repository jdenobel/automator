import os

from django.db import models

class DataFile(models.Model):
    path = models.CharField(max_length=3000, unique=True)
    name = models.TextField()
    
    @staticmethod
    def create_from_list(flist):
        return [
            DataFile.objects.get_or_create(
                path=fpath,
                name=os.path.basename(fpath)
            )[0]            
            for fpath in flist
        ]

    def __str__(self):   
        return self.name