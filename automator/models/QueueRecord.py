import os
import signal
import datetime

from django.db import models, transaction
from django.conf import settings
from django.utils import timezone
from django_celery_results.models import TaskResult
from django.core.exceptions import ObjectDoesNotExist

from automator.models import Notification
from core.celery_app import app
from celery.result import AsyncResult


class QueueRecord(models.Model):
    pid = models.IntegerField(null=True, blank=True)
    created_at = models.DateTimeField(default=timezone.now)
    pipeline_started_at = models.DateTimeField(null=True, blank=True)
    pipeline_completed_at = models.DateTimeField(null=True, blank=True)
    exit_code = models.IntegerField(null=True, blank=True)
    async_code = models.CharField(max_length=100, null=True, blank=True)
    status = models.CharField(max_length=20, default="PENDING")
    command = models.TextField(null=True, blank=True)
    priority = models.IntegerField(null=True, blank=True)
    required_cpus = models.IntegerField(null=True, blank=True)
    watcher_email = models.CharField(max_length=250, null=True, blank=True)
    started_by = models.CharField(max_length=250, default='Automator')

    @staticmethod
    def sync_statusses():
        two_weeks_ago = timezone.now() - datetime.timedelta(weeks=2)
        three_days_ago = timezone.now() - datetime.timedelta(days=3)
        with transaction.atomic():
            for r in TaskResult.objects.filter(
                    date_done__gt=three_days_ago):
                # sync status
                QueueRecord.objects.filter(
                    async_code=r.task_id).update(
                    status=r.status,
                    pipeline_completed_at=(
                        None if r.status in ["RETRY", "PENDING", "RUNNING"] else r.date_done)
                )
            # sync weird non terminating jobs
            QueueRecord.objects.filter(
                pipeline_started_at__lt=two_weeks_ago,
                pipeline_completed_at=None
            ).update(
                exit_code=42,
                pipeline_completed_at=timezone.now(),
                status="NOTERM"
            )

    @property
    def log_file(self):
        return os.path.join(
            settings.LOGS_ROOT, f"{self.async_code}"
        )

    @property
    def logs(self):
        if not os.path.isfile(self.log_file):
            return 'No Logfile Found!'
        with open(self.log_file, 'r') as log_file:
            return log_file.read()

    @property
    def current_task(self):
        return TaskResult.objects.filter(task_id=self.async_code).first()

    @property
    def splitted_command(self):
        if self.command:
            splitted = self.command.split("&&")
            if len(splitted) == 2:
                return (splitted[0], splitted[1],)
        return ("NA", self.command,)

    @property
    def working_directory(self):
        return self.splitted_command[0]

    @property
    def command_nice(self):
        return self.splitted_command[1]

    @property
    def assumed_name(self):
        try:
            for s in self.command_nice.split():
                if "." in s and "/" in s:
                    return s.split("/")[-1].split(".")[0]
        except:
            pass

    @property
    def assumed_project(self):
        try:
            return next(filter(lambda x: x.isdigit(),
                               self.command_nice.split()))
        except:
            pass

    @property
    def runtime(self):
        if self.pipeline_completed_at and self.pipeline_started_at:
            return str(self.pipeline_completed_at - self.pipeline_started_at)

    def final_check(self):
        try:
            if self.exit_code != 0 or 'err' in self.logs.lower() or "traceback" in self.logs.lower():
                self.email_status('ERROR')
            else:
                self.email_status('COMPLETED')
        except ObjectDoesNotExist:
            pass

    def email_status(self, status):
        try:
            subject = f"[{status}] Project: {self.order_id} Task: {self.process_name}"
            msg = (
                f"Status: {self.status}\nExit code: {self.exit_code}\n"
                f"Started at: {self.pipeline_started_at}\nCompleted at: "
                f"{self.pipeline_completed_at}\n\nSamples: "
                f"{' '.join(str(x) for x in self.samples.all()) }\n"
                f"Command: {self.command}\n\n"
            )

        except AttributeError:
            subject = f"[{status}] Queue: [{self.command[:60]}]"
            msg = (
                f"Status: {self.status}\nExit code: {self.exit_code}\n"
                f"Started at: {self.pipeline_started_at}\nCompleted at: "
                f"{self.pipeline_completed_at}\n\nCommand: {self.command}\n\n"
            )

        Notification.Notification.email_watchers(subject, msg, self)

    def revoke(self, user=''):
        if self.status in ("PENDING", "RUNNING", "RETRY",):
            try:
                if self.status == "RUNNING":
                    os.killpg(os.getpgid(self.pid), signal.SIGINT)
                if self.async_code:
                    app.control.revoke(self.async_code)

                self.status = f"REVOKED by {user.username}"
                self.pipeline_completed_at = timezone.now()
                self.exit_code = -1
                self.save()
                return True
            except Exception as error:
                print(f"Error whilst revoking {error}")
        return False
