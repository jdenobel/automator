import os 
from django.db import models
from django.contrib.auth.models import User, Group
# from django.core.mail import send_mail
from django.core.mail import EmailMessage
from django.conf import settings



class Notification(models.Model):
    """
    TODO: Add logs to mail as appendix
        mail.attach(image1.name, image1.read(), image1.content_type)

        Check if this doens't send to often

        Create screen for revoking,

        migrate to new queueing
    """
    user = models.ForeignKey(User, related_name='notifications', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    msg = models.TextField()
    subject = models.CharField(max_length=200)
    sent_email = models.BooleanField(default=False)
    queue_record = models.ForeignKey('QueueRecord', related_name='notifications', on_delete=models.CASCADE, default=None, null=True)

    def __str__(self):
        return f"[{self.created.strftime('%d-%b-%Y %H:%M')}] {self.subject}"

    def send(self):
        try:
            email = EmailMessage(
                f"{'[TEST] ' if settings.LOCAL else ''}{self.subject}",
                self.msg,
                settings.SERVER_EMAIL,
                [self.user.email],                
            )         
            if self.queue_record and self.queue_record.pipeline_completed_at:
                email.attach('logs.txt', self.queue_record.logs, 'text/plain')
            email.send(fail_silently=False)
            self.sent_email = True
            self.save()
        except Exception as error:
            print(error)


    @staticmethod
    def send_mail(subject, msg, user, queue_record=None):
        notification = Notification.objects.create(
            user=user,
            msg=msg,
            subject=subject,
            queue_record=queue_record
        ).send()

    @staticmethod
    def email_admins(subject, msg,  queue_record=None):
        for email in settings.ADMIN_MAILS:
            user = User.objects.filter(email=email).first()
            if not user: continue
            Notification.send_mail(subject, msg, user, queue_record)
            

    @staticmethod
    def email_watchers(subject, msg, queue_record=None):
        if queue_record and queue_record.watcher_email:
            user = User.objects.filter(email=queue_record.watcher_email).first()
            return Notification.send_mail(subject, msg, user, queue_record)

        for user in Group.objects.get(name="watchers").user_set.all():
            Notification.send_mail(subject, msg, user, queue_record)
        