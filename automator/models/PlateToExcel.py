import os
import shutil 
from django.db import models

from .DataFile import DataFile
from .easytrack import FlowCellSample

INDEX_SETS = [700001, 700002, 700003, 700004]

class PlateToExcel(DataFile):
    plate_id = models.CharField(max_length=200, unique=True)
    et_sample_id = models.IntegerField()
    target_path = models.CharField(max_length=1000, unique=True)
    checked = models.BooleanField(default=False)

    @property
    def et_sample(self):
        return FlowCellSample.FlowCellSample.objects.filter(
                    code=self.et_sample_id
                ).first()

    def save(self, *args, **kwargs):    
        self.target_path = self.target_path.replace(" ", "_")
        try:
            shutil.copy(self.path, self.target_path)
        except shutil.SameFileError: 
            pass
        super(PlateToExcel, self).save(*args, **kwargs)   

    @property
    def sheet_type(self):
        if 'F088' in self.name:
            return 'F088'
        elif self.index_tag in INDEX_SETS:
            return 'F089'
        else:
            return 'Nextera'

    @property
    def command_section(self):
        if self.sheet_type == 'F088':
            return f"'{self.plate_id}' '{self.target_path}'"
        else:
            return f"'{self.plate_id}' '{self.index_tag_key}' '{self.target_path}'"
    
    @property
    def index_tag_key(self):
        if self.index_tag in INDEX_SETS:
            return 'ABCD'[INDEX_SETS.index(self.index_tag)]
        return 'x'

    @property
    def index_tag(self):
        if self.et_sample:
            return self.et_sample.indextag


    def __str__(self):
        return f"[{self.plate_id}] --> {self.name}" 