from django.db import models

from .easytrack import FlowCellSample


class Sample(models.Model):
    sample_id = models.IntegerField()
    artikel_id = models.CharField(max_length=20)
    input_files = models.ManyToManyField('DataFile')

    class Meta:
        unique_together = (
            "sample_id",
            "artikel_id"
        )

    @property
    def flowcell_sample(self):
        return FlowCellSample.objects.filter(pk=self.sample_id).first()

    @property
    def type_prep(self):
        return self.flowcell_sample.type_prep

    def __str__(self):
        return str(self.sample_id)
