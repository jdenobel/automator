import os
from glob import glob
from itertools import chain
from django.db import models
from django.utils.functional import cached_property
from django.utils import timezone
from django.conf import settings

from .QueueRecord import QueueRecord
from .easytrack import FlowCell, FlowCellStats, NextGenQualityStats
from .MergeDirectory import MergeDirectory

from core import tasks


class DemultiplexingRecord(QueueRecord):
    flowcell_id = models.IntegerField()
    automatic = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        is_new = not bool(self.pk)
        super(DemultiplexingRecord, self).save(*args, **kwargs)
        if is_new:
            self.ensure_jira_tickets()

    def final_check(self):
        try:
            if self.exit_code != 0:
                self.email_status('ERROR')
            else:
                fc = self.flowcell
                fc.status = 'Run klaar'
                fc.save()
                self.upload_stats_to_et()
                self.set_jira_to_completed()
            if self.automatic:
                self.email_status("COMPLETED DEMUX")

        except Exception as err:
            print(err)

    def upload_stats_to_et(self):
        try:
            if self.flowcell.is_illumina:
                for sum_file in glob(
                        os.path.join(self.data_folder, "RUN_SUMMARY_SAV*xml")):
                    FlowCellStats.FlowCellStats.read_summary_file(sum_file)

                for sum_file in glob(
                        os.path.join(self.data_folder, "flowcell_summary*xml")):
                    NextGenQualityStats.NextGenQualityStats.read_summary_file(
                        sum_file)
            elif self.is_pacbio:
                for sum_file in glob(
                        os.path.join(self.data_folder, "smrtcell_summary_SQ*xml")):
                    NextGenQualityStats.NextGenQualityStats.read_summary_file(
                        sum_file)
        except:
            pass

    @property
    def data_folder(self):
        return self.flowcell.data_folder

    @property
    def all_tickets(self):
        tickets = self.flowcell.get_tickets()
        return tickets + list(chain([
            list(chain(t.fields.issuelinks + t.fields.subtasks)) for t in tickets
        ]))[0]

    def set_jira_to_completed(self):
        self.flowcell.update_tickets_to_status(
            status="Projects currently being analy",
            tickets=self.all_tickets)

    def set_jira_to_running(self):
        self.flowcell.update_tickets_to_status(
            status='Run In Progress',
            tickets=self.all_tickets)

    @cached_property
    def flowcell(self):
        return FlowCell.FlowCell.objects.get(pk=self.flowcell_id)

    @property
    def additional_sheets(self):
        if self.flowcell.linked_plates:
            ass = " ".join(
                [i.command_section for i in self.flowcell.linked_plates])
            return f' --additional-sheets {ass} '
        return ''

    @property
    def merge_dirs(self):
        qs = MergeDirectory.objects.filter(
            flowcell_id=self.flowcell.pk, use=True)
        if qs.exists():
            return f' --merge-dirs {" ".join([ str(x.project_id) + " " + str(x.path) for x in qs.all()])} '
        return ''

    @property
    def filters(self):
        filters = {
            project: (
                self.flowcell.samples
                    .filter(project__pk=project, datainmbklant__gt=0)
                    .aggregate(models.Min('datainmbklant'))
                    .get("datainmbklant__min")
            )
            for project in self.flowcell.parser.data_project_ids
        }
        if len(filters) > 0:
            return f'  --filters {" ".join([str(k) + " " + str(v) for k,v in filters.items() if v != None])} '
        return ''

    @cached_property
    def projects(self):
        return self.flowcell.parser.projects

    @cached_property
    def projects_db(self):
        return [x.project for x in filter(None, [
            self.flowcell.samples.filter(
                project__pk=project['Sample_Project']).first()
            for project in self.projects
        ])]

    @property
    def rivm_projects(self):
        rivm_projects = [str(x.opdrachtcode)
                         for x in self.projects_db if x.is_rivm]
        if len(rivm_projects) > 0 and self.flowcell.samples.exists():
            return f' --rivm-projects {" ".join(rivm_projects)} '
        return ''

    def get_command(self):
        if self.flowcell.is_illumina:
            return (
                f"illumina_demultiplexing {self.flowcell.samplesheet_file_path} -p" +
                ('' if self.flowcell.is_run_completed else ' --wait ') +
                self.additional_sheets + self.filters +
                self.merge_dirs + self.rivm_projects
            )
        if self.flowcell.is_pacbio:
            base_dir = os.path.dirname(self.flowcell.data_folder)
            return (
                f"cd {base_dir} && "
                f"python /mnt/nfs/pipelines/pacbio_demultiplexing/pacbio_demultiplex.py "
                f"-s {self.flowcell.samplesheet_file_path} -l {self.flowcell.data_folder}"
            )

    @property
    def process_name(self):
        if self.flowcell.is_illumina:
            return 'illumina demux'
        if self.flowcell.is_pacbio:
            return 'pacbio demux'
        if self.flowcell.is_ont:
            return 'ont demux'

    @property
    def orders(self):
        return list(map(lambda x: x.pk, self.flowcell.projects))

    @property
    def runcommand(self):
        if not self.command:
            self.command = self.get_command()
        return self.command

    def ensure_jira_tickets(self):
        try:
            if not self.flowcell.get_tickets():
                self.flowcell.generate_tickets()
                print("Created JIRA tickets for", str(self))
        except:
            print("Couldn't generate JIRA tickets for", str(self))

    def execute(self):
        if not settings.PROD:
            print(self.runcommand)
            return
        try:
            if not self.flowcell.samplesheet_file_path:
                self.flowcell.write_samplesheet()

            assert self.flowcell.samplesheet_file_exists, "No samplesheet found"
            self.async_code = tasks.excecute_system_commmand.delay(
                record_id=self.pk,
                command=self.runcommand,
                priority=30,
                required_cpus=20
            ).id
            self.pipeline_started_at = timezone.now()
            self.save()
            if self.automatic:
                self.email_status("SCHEDULED")
        except Exception as error:
            print(
                f"An exception has occured, please try again. "
                f"If this persisists,restart celery service (admin). "
                f"\nException message:\n{error}")
