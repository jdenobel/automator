import os

from django.db import models
from django.utils.functional import cached_property

from .easytrack import FlowCell, EasyTrackOrder


class MergeDirectory(models.Model):
    project_id = models.IntegerField()
    path = models.CharField(max_length=3000, unique=True)
    flowcell_id = models.IntegerField()
    use = models.BooleanField(default=False)

    @property
    def project(self):
        return EasyTrackOrder.EasyTrackOrder.objects.get(pk=self.project_id)
    
    @cached_property
    def flowcell(self):
        return FlowCell.FlowCell.objects.get(pk=self.flowcell_id)

    def __str__(self):
        return f"[{str(self.project.pk)}]: {os.path.basename(self.path)}" 
    