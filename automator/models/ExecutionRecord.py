import os
import sys
import itertools
import subprocess

from django.db import models
from django.conf import settings
from django.utils import timezone
from django_celery_results.models import TaskResult

from .Notification import Notification
from .easytrack import EasyTrackOrder, FlowCellSample
from .QueueRecord import QueueRecord
from core import tasks, celery_app


class ExecutionRecord(QueueRecord):
    samples = models.ManyToManyField('Sample')
    order_id = models.IntegerField()
    pipeline = models.ForeignKey(
        "automator.Pipeline", on_delete=models.CASCADE, null=True)

    @property
    def order(self):
        return EasyTrackOrder.EasyTrackOrder.objects.get(pk=self.order_id)

    @property
    def process_name(self):
        return self.pipeline.name if hasattr(self.pipeline, 'name') else "Na"

    @property
    def main_script(self):
        return os.path.join(
            settings.PIPELINE_ROOT, self.pipeline.main_script
        )

    @property
    def orders(self):
        return [self.order.pk]

    @property
    def working_dir(self):
        return self.order.data_location

    @property
    def otu_callback(self):
        if all(x.type_prep == "DNA – ITS" for x in self.samples.all()):
            return "-d ITS"
        return ""

    def option_callback(self, key):
        return {
            "otu_callback": self.otu_callback,
        }.get(key) or ''

    @property
    def run_command(self):
        if self.pipeline.per_sample:
            input_pattern = " ".join([
                sample.input_files.filter(
                    name__contains=self.pipeline.input_file_pattern
                ).first().path
                for sample in self.samples.all()
            ])
        else:
            input_pattern = os.path.join(
                self.working_dir, self.pipeline.input_folder)
            assert os.path.isdir(
                input_pattern), f'Input was not found!\n\t{input_pattern} does not exist'

        standard_options = self.option_callback(self.pipeline.option_callback)
        return (
            f"cd {self.working_dir} && "
            f"{self.pipeline.executable} {self.main_script} "
            f"{self.pipeline.project_arg} {self.order_id} "
            f"{standard_options} "
            f"{self.pipeline.file_arg} {input_pattern}"
        )

    def execute(self):
        if not os.path.isfile(self.main_script):
            raise FileNotFoundError(
                f'Main script not found ({self.main_script})'
            )
        try:
            if settings.PROD:
                self.async_code = tasks.excecute_system_commmand.delay(
                    record_id=self.pk,
                    command=self.run_command,
                    priority=30,
                    required_cpus=self.pipeline.required_cpus
                ).id
                self.command = self.run_command
                self.pipeline_started_at = timezone.now()
                self.save()
                self.email_status("SCHEDULED")
            else:
                print(self.run_command)

        except Exception as error:
            print(
                f"An exception has occured, please try again. If this persisists, "
                f"restart celery service (admin). \nException message:\n{error}"
            )

    def __str__(self):
        return f"{self.pipeline} [{self.pk}]"
