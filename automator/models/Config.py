from django.db import models
from solo.models import SingletonModel


class Config(SingletonModel):
    cleanup = models.BooleanField(default=True)
    automation = models.BooleanField(default=True)

    class Meta:
        verbose_name = "Site Configuration"
