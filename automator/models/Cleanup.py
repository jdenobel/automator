from datetime import timedelta

from django.db import models
from django.utils import timezone
from .easytrack import EasyTrackOrder


def plus_one_week():
    return timezone.now() + timedelta(weeks=1)


class Cleanup(models.Model):
    project_id = models.IntegerField(unique=True)
    date_detected = models.DateTimeField(auto_now_add=True)
    date_cleanup = models.DateTimeField(default=plus_one_week)
    is_cleaned = models.BooleanField(default=False)
    skip = models.BooleanField(default=False)

    @property
    def requires_cleaning(self):
        return timezone.now() > self.date_cleanup and not self.is_cleaned

    @property
    def project(self):
        return EasyTrackOrder.objects.get(opdrachtcode=self.project_id)

    @staticmethod
    def clean_all():
        for record in Cleanup.objects.filter(
                is_cleaned=False, skip=False, date_cleanup__lt=timezone.now()):
            record()

    def __str__(self):
        return f"Cleanup: [{self.project_id}: {self.date_cleanup}]"

    def __call__(self):
        folder = self.project.data_location
        print("Cleaning project", self.project_id, folder)
        # logic here
