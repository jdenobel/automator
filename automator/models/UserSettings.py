
from django.db import models
from django.contrib.auth.models import User
from ..service import JIRAService


class UserSettings(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name="settings")
    jira_token = models.CharField(max_length=200, blank=True)
    jira_project = models.CharField(max_length=20, blank=True)
    # AOlXsAfPK2QgrvXk8LVlD891

    @property
    def jira_service(self):
        if not hasattr(self, "_jira_service"):
            self._jira_service = JIRAService(self.user.email,
                                             self.jira_token, self.jira_project)
        return self._jira_service
