from django import forms

from automator.models import Pipeline, ETToken, Config


class PipelineForm(forms.ModelForm):
    class Meta:
        model = Pipeline
        exclude = []

    file_arg = forms.CharField(required=False)
    option_callback = forms.CharField(required=False)
    tokens = forms.ModelMultipleChoiceField(
        queryset=ETToken.objects.all(),
        help_text=(
            "These are the ET codes associated with a "
            "specific pipeline. This is the signal that "
            "triggers pipeline execution, given that the files "
            "required to start are available."
        )
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance:
            self.fields["tokens"].initial = \
                self.instance.tokens.values_list('pk', flat=True)


class ConfigForm(forms.ModelForm):
    class Meta:
        model = Config
        exclude = []

    # cleanup = forms.BooleanField(widget=forms.CheckboxInput(
    #     attrs={'onclick': 'submitConfig(this.form); '}))
    # automation = forms.BooleanField(widget=forms.CheckboxInput(
    #     attrs={'onclick': 'submitConfig(this.form);'}))
