# Generated by Django 2.1 on 2019-06-26 12:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('automator', '0024_auto_20190515_1721'),
    ]

    operations = [
        migrations.AddField(
            model_name='pipeline',
            name='per_sample',
            field=models.BooleanField(default=True),
        ),
    ]
