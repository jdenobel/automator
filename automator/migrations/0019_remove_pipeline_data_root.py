# Generated by Django 2.1 on 2019-05-01 12:34

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('automator', '0018_pipeline_data_root'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pipeline',
            name='data_root',
        ),
    ]
