# Generated by Django 2.1 on 2019-03-01 10:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('automator', '0004_auto_20181220_1809'),
    ]

    operations = [
        migrations.AddField(
            model_name='platetoexcel',
            name='checked',
            field=models.BooleanField(default=False),
        ),
    ]
