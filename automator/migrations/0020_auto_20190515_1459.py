# Generated by Django 2.1 on 2019-05-15 12:59

from automator.models import plus_one_week
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('automator', '0019_remove_pipeline_data_root'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cleanup',
            fields=[
                ('id', models.AutoField(auto_created=True,
                                        primary_key=True, serialize=False, verbose_name='ID')),
                ('project_id', models.IntegerField(unique=True)),
                ('date_detected', models.DateTimeField(auto_now_add=True)),
                ('date_cleanup', models.DateTimeField(default=plus_one_week)),
            ],
        ),
        migrations.DeleteModel(
            name='OptionCallback',
        ),
    ]
