import os
from time import sleep, time
from datetime import datetime

from django.core.management.base import BaseCommand

from core.utils import timeit
from automator.models import *


class Command(BaseCommand):  
    @timeit
    def handle(self, *args, **options): 
        f = FlowCell.objects.get(volgnummer=1788)
        print(f.data_folder)
        print()
        print(f.samplesheet)
        import pdb; pdb.set_trace()