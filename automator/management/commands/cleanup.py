import os
from datetime import timedelta
from itertools import chain
from django.core.management.base import BaseCommand
from django.utils import timezone
from django.conf import settings

from core.utils import timeit
from automator.models import *


class Command(BaseCommand):
    def __init__(self):
        self.three_months_ago = timezone.now() - timedelta(weeks=12)

    @timeit
    def handle(self, *args, **options):
        self.__cleanup_ngs_projects()
        self.__cleanup_pacbio_data()
        self.__cleanup_ont()

        if Config.get_solo().cleanup:
            print("cleanup on")  # Turn on clean
            Cleanup.clean_all()
        else:
            print("no clean")

    def get_folders(self, path):
        return list(filter(lambda x: os.path.isdir(
            os.path.join(path, x)), os.listdir(path)))

    def __cleanup_ngs_projects(self):
        folders = self.get_folders(settings.DATA_ROOT)
        projects = set(map(
            lambda x: os.path.basename(x).split("_")[0], folders))
        qs = EasyTrackOrder.objects.filter(
            pk__in=projects, leverdatum__lt=self.three_months_ago)
        for o in qs:
            Cleanup.objects.get_or_create(project_id=o.pk)
        print("NGS-Projects: Need to cleanup", qs.count())

    def cleanup_projects_for_flowcells(self, flowcells):
        projects = list(filter(lambda x: x.leverdatum and x.leverdatum < self.three_months_ago,
                               filter(None, chain.from_iterable(f.projects for f in flowcells if f.is_demultiplexed))))
        print(len(projects))
        for p in projects:
            Cleanup.objects.get_or_create(project_id=p.pk)

    def __cleanup_ont(self):
        flowcells = FlowCell.objects.filter(sequencernaam="MinIon01")
        self.cleanup_projects_for_flowcells(flowcells)

    def __cleanup_pacbio_data(self):
        flowcells = FlowCell.objects.filter(sequencernaam='PacBio')

        self.cleanup_projects_for_flowcells(flowcells)
