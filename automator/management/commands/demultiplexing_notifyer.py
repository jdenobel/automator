import os
from time import sleep, time
from datetime import datetime

from django.core.management.base import BaseCommand

from core.utils import timeit
from automator.models import (
    FlowCell,   
    Notification,
    ILLUMINA_TYPES    
)

EMAIL_TEMPLATE = (
    "Dear People,\n\n"
    "There are some demux project that require samplesheets "
    "to be linked to them. See the list below:\n\n"
    "Volgnummer, IlluminaID, Plates\n"
    "{need_plates}\n\n"
    "Kind regards,\n"
    "Samuel L. Jackson"
)


class Command(BaseCommand):  
    #    gio mount smb://10.150.0.13/algemeen$/
    @timeit
    def handle(self, *args, **options): 
        need_plates = []
        for f in FlowCell.objects.filter(
            status="Sequencing", typesequencer__in=ILLUMINA_TYPES):
            if f.data_folder and bool(f.unseen_plates_repr) \
                and not f.is_demultiplexed:
                need_plates.append(
                    ", ".join((str(f.pk), f.illuminaid_nice, ", ".join(f.unseen_plates_repr)))
                )

        if bool(need_plates):
            Notification.email_watchers(
                subject="Check Demux Plates",
                msg=EMAIL_TEMPLATE.format(
                    need_plates="\n".join(need_plates)
                )
            )
