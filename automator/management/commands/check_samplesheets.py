import os
from glob import glob
from django.core.management.base import BaseCommand
import warnings
from core.utils import timeit
from automator.models import (
    FlowCell, FlowCellSample
)

from pipelinelib.utils.file import (
    F088, F089, Nextera
)

INDEX_SETS = [700001, 700002, 700003, 700004]


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('volgnummer', type=int)
        parser.add_argument('--folder', type=str, required=False, default=None)

    def get_path(self, plate_id, directory):
        base_msg = "Please give a path for a sample sheet for {}\n> ".format(
            plate_id)
        if not directory:
            return input(base_msg)

        xlsx_files = glob(os.path.join(directory, "*.xlsx"))
        for i, f in enumerate(xlsx_files):
            if i == 0:
                print("Available xlsx files in data folder of flowcell:")
            print("{}: {}".format(i, os.path.basename(f)))

        selection = input(
            base_msg if not any(xlsx_files) else (
                base_msg[:-2] +
                "Or select an integer option from the above list \n> "
            )
        )
        if selection.isdigit() and int(selection) in range(len(xlsx_files)):
            return xlsx_files[int(selection)]

        return os.path.join(directory, selection)

    def check_file_exists(self, plate_id, directory):
        filepath = self.get_path(plate_id, directory)
        while not os.path.isfile(filepath):
            filepath = self.get_path(plate_id, directory)
        return filepath

    def get_index_tag_key(self, index_tag):
        if index_tag in INDEX_SETS:
            return 'ABCD'[INDEX_SETS.index(index_tag)]
        return None

    def handle(self, *args, **options):
        with warnings.catch_warnings():
            warnings.simplefilter('ignore', UserWarning)
            return self.handle_(*args, **options)

    @timeit
    def handle_(self, *args, **options):
        flowcell = FlowCell.objects.get(volgnummer=options.get('volgnummer'))
        print("Flowcell {} has {} unseen plates".format(
            flowcell, len(flowcell.parser.unseen_plates)))
        exceptions = []
        if os.path.exists(flowcell.data_folder or ''):
            directory = flowcell.data_folder
        else:
            directory = options.get("folder")

        while bool(flowcell.unseen_plates):
            print(flowcell.unseen_plates, "\n")
            plate_id = flowcell.unseen_plates[0]
            sample = FlowCellSample.objects.filter(
                code=int(plate_id.split("_")[-1])
            ).first()
            index_key = self.get_index_tag_key(sample.indextag)
            sheet_path = self.check_file_exists(plate_id, directory)
            filename = os.path.basename(sheet_path)
            try:
                if 'F088' in filename:
                    flowcell.parser.update_data(
                        F088(sheet_path, plate_id=plate_id))
                elif index_key:
                    flowcell.parser.update_data(
                        F089(
                            sheet_path,
                            plate_id=plate_id,
                            index_set_id=index_key
                        )
                    )
                else:
                    flowcell.parser.update_data(
                        Nextera(sheet_path, plate_id=plate_id,
                                custom_tags=None)
                    )
                print(filename, "passes checks")
            except Exception as err:
                exceptions.append(
                    [plate_id, err]
                )
                print(f"An error happened: {str(err)}")
                import traceback
                print(traceback.print_exc())
            print("\n")

        if any(exceptions):
            print("#"*80)
            print("There were exceptions:")
            for e in exceptions:
                print("Plate:{}\nerror{}\n".format(*e))
            raise Exception("There were errors in one of the samplesheets")

        print(flowcell.parser.data.to_string())
        print("*"*80, "\n")

        if os.path.exists(directory or ''):
            choice = input("Do you want to write this samplesheet to {}\n (Y/n) > ".format(
                directory
            )).lower()
            if choice == "y":
                flowcell.parser.write_raw(directory=directory)
