#!/usr/local/bin/python3
import sys
import os


if __name__ == "__main__":
    from subprocess import run
    from socket import gethostname
    base = '/home/jacob/Code/' if gethostname() == "rekenmachine" else '/mnt/nfs/'
    pyth = "python3" if gethostname() == "rekenmachine" else '/usr/local/bin/python3'
    venvpy = os.path.join(base, "automator/venv/bin/python")
    pyth = venvpy if os.path.isfile(venvpy) else pyth
    run([pyth, f"{base}automator/manage.py", "Q"] + sys.argv[1:])
else:
    import getpass
    from argparse import ArgumentTypeError

    from django.core.management.base import BaseCommand, handle_default_options, CommandError
    from django.core.exceptions import ImproperlyConfigured
    from django.db import connections
    from django.conf import settings

    from core.tasks import excecute_system_commmand
    from automator.models import QueueRecord

    class Command(BaseCommand):

        def add_arguments(self, parser):
            parser.add_argument("--test", action="store_true",
                                required=False, help="Run test command")
            parser.add_argument("--verbose", action="store_true",
                                required=False, help="Set verbosity")
            parser.add_argument("--priority", type=int, default=30,
                                help="Set priority [1(high) - 30 (low)]")
            parser.add_argument("--max-required-cpus", type=int, default=6,
                                help="The maximum ammount of CPUs used by the command")
            parser.add_argument(
                "--working-dir", help="The working directory of the command")

        def run_from_argv(self, argv):
            self._called_from_command_line = True
            parser = self.create_parser(argv[0], argv[1])
            options, extra = parser.parse_known_args(argv[2:])
            setattr(options, 'extra', extra)
            cmd_options = vars(options)
            args = cmd_options.pop('args', ())
            handle_default_options(options)
            try:
                self.execute(*args, **cmd_options)
            except Exception as e:
                if options.traceback or not isinstance(e, CommandError):
                    raise
                self.stderr.write('%s: %s' % (e.__class__.__name__, e))
                sys.exit(1)
            finally:
                try:
                    connections.close_all()
                except ImproperlyConfigured:
                    pass

        def test(self):
            record = QueueRecord.objects.create(
                command="docker service create --reserve-cpu 2 --restart-condition none ubuntu sleep 10",
                priority=1,
                required_cpus=1,
            )
            excecute_system_commmand.delay(
                record_id=record.pk,
                command=record.command,
                priority=record.priority,
                required_cpus=record.required_cpus)

        def echo(self, msg):
            if self.verbose:
                print(msg)

        def handle(self, *args, **options):
            self.verbose = options.get("verbose")
            if options.get("test"):
                self.test()
            elif len(options.get("extra")) == 0:
                raise ArgumentTypeError('Command must be provided')

            if options.get("max_required_cpus") > settings.TOTAL_CPUS:
                raise ArgumentTypeError(
                    f"Can't run with more than {settings.TOTAL_CPUS} CPUs")

            self.echo(f"Running priority {options.get('priority')}")
            self.echo(
                f"Running with max CPUs {options.get('max_required_cpus')}")
            command = ' '.join(options.get("extra"))

            if options.get("working_dir"):
                command = f"cd {options.get('working_dir')} && {command}"
            else:
                command = f"cd {os.getcwd()} && {command}"

            self.echo(f"Command:\n\t{command}")
            system_user = getpass.getuser()
            try:
                record = QueueRecord.objects.create(
                    command=command,
                    priority=options.get("priority"),
                    required_cpus=options.get("max_required_cpus"),
                    started_by=system_user,
                    watcher_email=settings.LINUX_USERNAME_MAPPER.get(
                        system_user),

                )
                async_code = excecute_system_commmand.delay(
                    record_id=record.pk,
                    command=record.command,
                    priority=record.priority,
                    required_cpus=record.required_cpus
                )
                record.async_code = async_code
                record.save()

                print("Succesfully queued job", str(record.id))
            except Exception as error:
                print(
                    f"An exception has occured, please try again. If this persisists,"
                    f"restart celery service (admin). \nException message:\n{error}"
                )
