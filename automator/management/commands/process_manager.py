import os
from time import sleep, time

from django.core.management.base import BaseCommand
from django.conf import settings
from django.db.models import Q
from core.utils import timeit
from automator.models import (
    FlowCell,
    FlowCellSample,
    OrderArtikel,
    EasyTrackOrder,
    ExecutionRecord,
    DataFile,
    Notification,
    Sample,
    QueueRecord,
    DemultiplexingRecord,
    ILLUMINA_TYPES,
    ETToken,
    Config
)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--delete', action="store_true")
        parser.add_argument('--verbose', action="store_true", default=False)
        parser.add_argument("--check", type=int, default=None)

    @timeit
    def handle(self, *args, **options):
        assert os.path.isdir(settings.ILLUMINA_DATA_ROOT), \
            f'Could not acces data root at {settings.ILLUMINA_DATA_ROOT}'
        self.verbose = options.get('verbose')
        if options.get('delete'):
            self.__delete_all_models()
        elif options.get("check"):
            self.__check_er(options.get("check"))
        else:
            try:
                if Config.get_solo().automation:
                    self.__queue_records()
                    self.__start_pipelines()
                    self.__start_demux()
                QueueRecord.sync_statusses()
            except Exception as error:
                if settings.PROD:
                    Notification.email_admins(
                        "Error ocurred in process manager", str(error),
                    )
                raise error

    def echo(self, *args, **kwargs):
        if self.verbose:
            print(*args, **kwargs)

    def __start_pipelines(self):
        for record in ExecutionRecord.objects\
                .filter(pipeline_started_at=None, pipeline_completed_at=None):
            try:
                record.execute()
            except FileNotFoundError as error:
                self.echo(error)
                continue

    def __start_demux(self):
        for f in FlowCell.objects.filter(status="Sequencing")\
            .filter(
                Q(typesequencer__in=ILLUMINA_TYPES) | Q(sequencernaam='PacBio')):
            try:
                if f.can_be_automated:

                    self.echo("Starting", f.volgnummer)
                    DemultiplexingRecord.objects.create(
                        flowcell_id=f.pk).execute()
            except Exception as error:
                self.echo("Skipping", f.volgnummer, "error", str(error))

    def __check_er(self, record_id):
        record = ExecutionRecord.objects.get(pk=record_id)
        artikel = OrderArtikel.objects.get(
            produktnummer=record.artikel_id,
            opdrachtcode=record.order_id)

        samples = FlowCellSample.objects.filter(
            code__in=[s.sample_id for s in record.samples.all()])
        print("Code, Naam, Flowcell, Artikel, Output folder?, is performed?")
        for sample in samples:
            print(
                sample.code,
                sample.naam,
                sample.volgnummer,
                artikel.produktnummer,
                artikel.has_output_folder(sample),
                artikel.is_performed(sample),
            )

    def __delete_all_models(self):
        for model in (ExecutionRecord, DataFile, Sample):
            model.objects.all().delete()

    def __queue_records(self):
        orders = [
            x for x in EasyTrackOrder.objects.filter(
                opdrachtcode__in=FlowCellSample.get_completed_flowcells()
            ).exclude(dienst="Next Gen Custom") if x.can_be_used
        ]

        for order in orders:
            self.echo(order, order.artikelen, len(order.samples))
            for artikel in order.artikelen:
                sample_records = []
                pipeline = artikel.pipeline
                for sample in order.samples:
                    try:
                        # Check for input presence
                        input_files = DataFile.create_from_list(
                            sample.get_input_files(
                                pipeline.input_file_ext,
                                pipeline.input_file_pattern,
                                pipeline.input_folder,
                                pipeline.n_input_files
                            )
                        )
                    except FileNotFoundError as error:
                        self.echo(error)
                        continue

                    if not artikel.is_performed(sample):
                        sample_record, _ = Sample.objects.get_or_create(
                            sample_id=sample.pk,
                            artikel_id=artikel.produktnummer
                        )
                        sample_record.input_files.add(*input_files)
                        sample_records.append(sample_record)

                if len(sample_records) != 0:
                    record = ExecutionRecord.objects.create(
                        pipeline=pipeline,
                        order_id=order.pk,
                    )
                    record.samples.add(*sample_records)
