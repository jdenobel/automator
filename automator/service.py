from collections import defaultdict
import datetime

from django.db.models import Q

from jira import JIRA, Issue
from .models.easytrack import FlowCell
from typing import Dict, Tuple, List, Optional, Union


class JIRAService:
    SERVER = "https://baseclear.atlassian.net"

    client = None
    custom_field_mapper = {
        'end_date': 'customfield_10926',
        'baseline_end_date': 'customfield_10923',
        'run_date': 'customfield_10943',
        'project_owner': 'customfield_10947',
    }

    def __init__(self, username, token, project_key=None):
        self.client = JIRA(
            server=JIRAService.SERVER, 
            basic_auth=(username, token),
            max_retries=2,
        )

        if project_key:
            self.select_project(project_key)
    
    @staticmethod
    def get_instance_for_user(user) -> 'JIRAService':
        # USER = "coen.mulders@baseclear.nl"
        # TOKEN = "x5iu61nxQnfSy68nd6Sx2EA6"
        return JIRAService(user.email, user.profile.jira_token, user.profile.jira_project)

    def select_project(self, proj_key: str):
        self.project = self.client.project(proj_key)

    def generate_tickets(self, flowcell: FlowCell, dry_run: bool = False) -> List:
        generator = FlowCellTicketGenerator(self)
        tickets = generator.generate_tickets(flowcell)
        # tickets = self.client.search_issues('key in (TC-128, TC-91, TC-94, TC-95, TC-96, TC-99, TC-102, TC-104, TC-105, TC-107)')
        return  self.add_url(tickets)

    def add_url(self, tickets):
        for ticket in tickets:
            ticket.url = self.get_url_for_ticket(ticket)
            ticket.kanban_url = self.get_kanban_url_for_ticket(ticket)
        return tickets

    def get_tickets(self, flowcell):
        tickets = self.search_issues(f"FC {flowcell.pk}", multiple=True)
        return self.add_url(tickets)

    def get_url_for_ticket(self, ticket):
        return '/'.join([self.client.client_info(), 'browse', ticket.key])

    def get_kanban_url_for_ticket(self, ticket):
        return '/'.join([self.client.client_info(), 'secure', f"RapidBoard.jspa?projectKey={ticket.fields.project.key}&search={ticket.key}"])

    def rename_fields(self, fields):
        # Rename custom field
        for old, new in self.custom_field_mapper.items():
            if old in fields:
                fields[new] = fields.get(old)
                del fields[old]

    def update_ticket(self, ticket: Issue, fields) -> Issue:
        self.rename_fields(fields)
        ticket.update(fields=fields)
        return ticket

    def set_status_for_ticket(self, ticket, status):
        trans_id = self.client.find_transitionid_by_name(ticket.key, status)
        if trans_id:
            return self.client.transition_issue(ticket.key, trans_id)    
        

    def create_task(self, fields) -> Issue:
        fields.update({
            'project': self.project.key, 
            'issuetype': {'id': 10101},
        })
        self.rename_fields(fields)
        new_ticket = self.client.create_issue(fields)
        new_ticket.new = True
        self.set_status_for_ticket(new_ticket, 'scheduled')
        return new_ticket
    
    def create_subtasks(self, parent: Issue, field_list):
        for fields in field_list:
            fields.update({
                'project': self.project.key, 
                'issuetype': {'id': 10102},
                'parent': {'key': parent.key}
            })
            self.rename_fields(fields)

        return list(self.client.create_issues(field_list))

    def link_tickets(self, inwardTicket: Issue, outwardTickets: List[Issue]):
        for outward in outwardTickets:
            self.client.create_issue_link("Relates", inwardTicket.key, outward.key)

    def try_search_user(self, username):
        users = self.client.search_users(username)
        if not users:
            return
        return users[0]

    def search_issues(self, keywords: Union[str, List[str]], iss_type=10101, multiple=False, options=dict()) -> Optional[Union[Issue, List[Issue]]]:
        if isinstance(keywords, List):
            keywords = " OR ".join(keywords)
        
        jql = f'project = {self.project} AND issuetype = {iss_type} AND (summary ~"{keywords}") ORDER BY created DESC'
        issues = self.client.search_issues(jql, maxResults=2, **options)
        
        if multiple:
            # Just return everything
            return issues
        
        if len(issues) > 1:
            raise Exception('Multiple issues found')
        
        return next(issues, None)

class FlowCellTicketGenerator:
    def __init__(self, service: JIRAService):
        self.service = service

    def generate_tickets(self, flowcell: FlowCell) -> List:
        """Generates a list of tickets and sub-tickets
        """
        flowcell_ticket = self.flowcell_ticket(flowcell)
        project_tickets = self.project_tickets(flowcell.projects)

        if flowcell_ticket and project_tickets:
            self.service.link_tickets(flowcell_ticket, project_tickets)

        all_tickets = [flowcell_ticket] + project_tickets
        
        return all_tickets

    def search_project_tickets(self, projects) -> Dict[int, Issue]:
        project_id = [p.pk for p in projects]
        ticket_map = self.service.search_many([str(p) for p in project_id], {'fields': 'summary'})
        return {
            int(k): v
            for k, v in ticket_map.items()
        }
            
    def flowcell_ticket(self, flowcell: FlowCell):
        run_datum = getattr(flowcell, 'rundatum', None)
        data = {
            "typesequencer": flowcell.typesequencer,
            "typerun": flowcell.typerun,
            "runlengte": flowcell.runlengte,
            "volgnummer": flowcell.volgnummer,
            "status": flowcell.status if flowcell.status else "",
            "rundatum": flowcell.rundatum if flowcell.rundatum else datetime.datetime.now(),
            "sequencernaam": flowcell.sequencernaam if flowcell.sequencernaam else "",
            "typesequencer": flowcell.typesequencer,
            "illuminaid": flowcell.illuminaid if flowcell.illuminaid else "",
            "opmerkingen": flowcell.opmerkingen if flowcell.opmerkingen else "",
        }
        summary_str = '{typesequencer}: {typerun} {runlengte} (FC {volgnummer})'.format(**data)
        description_str = 'Status: {status}\n Rundatum: {rundatum:%Y-%m-%d}\nSequencer: {sequencernaam}\nType Sequencer: {typesequencer}\nIlluminaID: {illuminaid}\n\n{opmerkingen}'.format(**data)

        ticket = self.service.create_task({
            'summary': summary_str,
            'description': description_str,
            'run_date': run_datum.isoformat() if run_datum else None,
        })

        subtasks = self.flowcell_subtasks(flowcell.typesequencer)
        if subtasks:
            self.service.create_subtasks(ticket, subtasks)

        return ticket

    def flowcell_subtasks(self, ftype: str):
        if ftype in ('HF', 'MS', 'S1', 'S2', 'S3', 'S4', 'SP'): # HiSeq, MiSeq, NovaSeq -> illumnia
            return [
                {'summary': 'bcl2fastq'},
                {'summary': 'check run quality'},
                {'summary': 'check undetermined'},
                {'summary': 'check data per sample'},
                {'summary': 'upload stats ET'},
            ]
        elif ftype == 'PB': # PacBio
            return [
                {'summary': 'download'},
                {'summary': 'demultiplex'},
                {'summary': 'blast check'},
                {'summary': 'upload stats ET'},
            ]
        elif ftype == 'NM': # NanoPore (Minion)
            return [
                # {'summary': 'download'},
            ]
        else:
            raise Exception(f'Undefined flowcell type {ftype}')

    def project_tickets(self, projects):
        return [self.project_task(p) for p in projects]

    def make_project_title(self, project) -> List[str]:
        project_title = [f'{project.opdrachtcode:0>6}']
        project_title.append(project.klantnummer.ticketname)
        
        if project.is_custom:
            project_title.append('CUSTOM')
        
        if project.is_premium:
            project_title.append('PREMIUM')
        
        return project_title

    def project_task(self, project):
        project_title = self.make_project_title(project)
        owner = self.service.try_search_user(project.projecteigenaar)

        fields = {
            'summary': " ".join(project_title),
            'description': f'{project.nextgen.bioinformaticaopmerkingen}',
            'end_date': project.verwachte_einddatum.isoformat(),
            'project_owner': {'name': owner.displayName} if owner else None,
        }

        fields.update({'baseline_end_date': fields.get('end_date')})
        existing = self.service.search_issues(project_title[0])
        if isinstance(existing, Issue):
            # Update existing issue
            return self.service.update_ticket(existing, fields)
        
        ticket = self.service.create_task(fields)

        subtasks = self.project_subtasks(project)
        if subtasks:
            self.service.create_subtasks(ticket, subtasks)

        return ticket

    def project_subtasks(self, project):
        subtasks = [
            {
                'summary': '{hoeveelheid}x {produktnummer}: {produktomschrijving}'.format(**artikel.__dict__),
                #'description': 'Test',
            }
            for artikel in project.bioinf_artikelen
        ]
        subtasks.append({'summary': 'quality statistics checked'})

        # Group the samples per required MB 
        datainmb_dict = defaultdict(list)
        for s in project.nextgensamples.filter(samplestatus__in=['Gesequenced', 'Sequencing']).all():
            datainmb = s.datainmb or 0
            datainmb_dict[datainmb].append(s)
        
        sample_table_headers = ["Naam", "Species", "Status"]

        sample_tasks = [
            {
                'summary': f'{len(samples)}x {mb}MB',
                'description': self.make_table(
                    sample_table_headers, 
                    [
                        [s.naam or 'No name', s.species or 'Unknown species', s.samplestatus or 'Unknown status']
                        for s in samples
                    ]
                ),
            }
            for mb, samples in datainmb_dict.items()
        ]
        return subtasks + sample_tasks
    
    def make_table(self, headers: list, rows: List[list]):
        table = ["||" + "||".join(headers) + "||"]
        table.extend(["|" + "|".join(r) + "|" for r in rows])
        return "\n".join(table)
